from gFTL

#Vector Methods

size: Vector->Int

capacity Vector->Int

empty: Vector->Int

at_size_kind: Vector->int->???->Pointer?
    integer, optional, intent(out) :: rc
     __type_declare_result, pointer :: res

at_32: Vector->int->??->Pointer?
    type (KeywordEnforcer), optional, intent(in) :: unused
     __type_declare_result, pointer :: res

of: Vector->Int->Pointer?
     __type_declare_result, pointer :: res

get_size_kind: Vector->Int->Pointer`?
    __type_declare_result, pointer :: res

get_32: Vector->Int->Pointer?
     __type_declare_result, pointer :: res

get_data:Vector->Pointer?
     __type_declare_result, pointer :: res

back:Vector->Pointer?
     __type_declare_result, pointer :: res

front:Vector->Pointer?
     __type_declare_result, pointer :: res

set_size_kind:Vector->Int->T->Void

set_32:Vector->Int->T->Void

reset: Vector->Void

copyFromArray: Vector->Array[T]->Void

get_index: Vector->T->Int

equal: Vector->Vector->Int

not_equal:Vector->Vector-Int

push_back:Vector->T->???->Int->Void
     type (KeywordEnforcer), optional, intent(in) :: unused

pop_back: Vector->Void

insert_size_kind:Vector->Int->T->???->Int->Void
     type (KeywordEnforcer), optional, intent(in) :: unused

insert_32: Vector->Int->T->Void

resize_size: Vector->Int->T->???->Int->Void
     type (KeywordEnforcer), optional, intent(in) :: unused

resize_32: Vector->Int->T?->???->Int->Void
     type (KeywordEnforcer), optional, intent(in) :: unused

downsize:Vector->Int->Void

clear: Vector->Void

shrink_to_fit:Vector->Void

erase_one:Vector->??->Void
     type (__iterator), intent(in)  :: position

erase_range:Vector->Int?->Int?->Void
     type (__iterator), intent(in)  :: first
     type (__iterator), intent(in)  :: last

reserve_size_kind: Vector->Int->Void

reserve_32:Vector->Int->Void

set_capacity:Vector->Int->Void

grow_to:Vector->Int->Void

swap:Vector->Vector->Void