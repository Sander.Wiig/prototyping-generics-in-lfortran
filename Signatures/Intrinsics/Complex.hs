module Complex where
import CommonDefinitions
import XASR
import qualified ASR

--pain
complexSignature::ParentSymbolTable->Program->Symbol
complexSignature parent program= Signature parent "Complex" [
                                                        --Add | Sub | Mul | Div | Pow
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just 0) "Complex" []) Intrinsic Public Required False),
                                                                        ("right", Variable addID "right" In Nothing Nothing Parameter (DefinableType (Just 0) "Complex" []) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just 0) "Complex" []) Intrinsic Public Required False)
                                                                     ]) "Complex_Add"[
                                                                         Var addID "left",
                                                                         Var addID "Right"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just 0) "Complex" []) Intrinsic Public Required False),
                                                                        ("right", Variable addID "right" In Nothing Nothing Parameter (DefinableType (Just 0) "Complex" []) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just 0) "Complex" []) Intrinsic Public Required False)
                                                                     ]) "Complex_Sub"[
                                                                         Var addID "left",
                                                                         Var addID "Right"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just 0) "Complex" []) Intrinsic Public Required False),
                                                                        ("right", Variable addID "right" In Nothing Nothing Parameter (DefinableType (Just 0) "Complex" []) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just 0) "Complex" []) Intrinsic Public Required False)
                                                                     ]) "Complex_Mul"[
                                                                         Var addID "left",
                                                                         Var addID "Right"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just 0) "Complex" []) Intrinsic Public Required False),
                                                                        ("right", Variable addID "right" In Nothing Nothing Parameter (DefinableType (Just 0) "Complex" []) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just 0) "Complex" []) Intrinsic Public Required False)
                                                                     ]) "Complex_Div"[
                                                                         Var addID "left",
                                                                         Var addID "Right"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just 0) "Complex" []) Intrinsic Public Required False),
                                                                        ("right", Variable addID "right" In Nothing Nothing Parameter (DefinableType (Just 0) "Complex" []) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just 0) "Complex" []) Intrinsic Public Required False)
                                                                     ]) "Complex_Pow"[
                                                                         Var addID "left",
                                                                         Var addID "Right"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        --Invert | Not | UAdd | USub
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just 0) "Complex" []) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just 0) "Complex" []) Intrinsic Public Required False)
                                                                     ]) "Complex_UAdd"[
                                                                         Var addID "left"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just 0) "Complex" []) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just 0) "Complex" []) Intrinsic Public Required False)
                                                                     ]) "Complex_USub"[
                                                                         Var addID "left"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        --Eq | NotEq | Lt | LtE | Gt | GtE
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just 0) "Complex" []) Intrinsic Public Required False),
                                                                        ("right", Variable addID "right" In Nothing Nothing Parameter (DefinableType (Just 0) "Complex" []) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just 0) "Integer" []) Intrinsic Public Required False)
                                                                     ]) "Complex_Eq"[
                                                                         Var addID "left",
                                                                         Var addID "Right"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just 0) "Complex" []) Intrinsic Public Required False),
                                                                        ("right", Variable addID "right" In Nothing Nothing Parameter (DefinableType (Just 0) "Complex" []) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just 0) "Integer" []) Intrinsic Public Required False)
                                                                     ]) "Complex_NotEq"[
                                                                         Var addID "left",
                                                                         Var addID "Right"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just 0) "Complex" []) Intrinsic Public Required False),
                                                                        ("right", Variable addID "right" In Nothing Nothing Parameter (DefinableType (Just 0) "Complex" []) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just 0) "Integer" []) Intrinsic Public Required False)
                                                                     ]) "Complex_Lt"[
                                                                         Var addID "left",
                                                                         Var addID "Right"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just 0) "Complex" []) Intrinsic Public Required False),
                                                                        ("right", Variable addID "right" In Nothing Nothing Parameter (DefinableType (Just 0) "Complex" []) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just 0) "Integer" []) Intrinsic Public Required False)
                                                                     ]) "Complex_LtE"[
                                                                         Var addID "left",
                                                                         Var addID "Right"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just 0) "Complex" []) Intrinsic Public Required False),
                                                                        ("right", Variable addID "right" In Nothing Nothing Parameter (DefinableType (Just 0) "Complex" []) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just 0) "Integer" []) Intrinsic Public Required False)
                                                                     ]) "Complex_Gt"[
                                                                         Var addID "left",
                                                                         Var addID "Right"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just 0) "Complex" []) Intrinsic Public Required False),
                                                                        ("right", Variable addID "right" In Nothing Nothing Parameter (DefinableType (Just 0) "Complex" []) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just 0) "Integer" []) Intrinsic Public Required False)
                                                                     ]) "Complex_GtE"[
                                                                         Var addID "left",
                                                                         Var addID "Right"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing
                                                        ]
                                                    where 
                                                        addID = getNextSymbolTableID program