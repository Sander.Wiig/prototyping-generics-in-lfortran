module Tests where 
import Transform
import ASR
import InverseTransform
import qualified XASR
import CommonDefinitions


expr1 = (TranslationUnit (SymbolTable (Nothing) 1 [("expr2", (Program (SymbolTable (Just 1) 2 [("x", (Variable 2 "x" Local (Nothing) (Nothing) Default (Integer 4 []) Source Public Required False))]) "expr2" [] [(Assignment (Var 2 "x") (BinOp (BinOp (ConstantInteger 2 (Integer 4 [])) Add (ConstantInteger 3 (Integer 4 [])) (Integer 4 []) (Just (ConstantInteger 5 (Integer 4 []))) (Nothing)) Mul (ConstantInteger 5 (Integer 4 [])) (Integer 4 []) (Just (ConstantInteger 25 (Integer 4 [])) )(Nothing)) (Nothing))]))]) [])
test1 = transform expr1
test2 = inverseTransform $ transform expr1
