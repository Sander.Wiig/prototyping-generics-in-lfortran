pure subroutine insertion_sort( array )
!Bog standard insertion sort.
    integer, intent(inout) :: array(0:)
    integer :: i, j
    integer :: key
    do j=0, size(array, kind=0) - 1
        key = array(j)
        i = j - 1
        do while( i >= 0 )
            if (array(i) == key ) exit
            array(i+1) = array(i)
            i = i - 1
        end do
        array(i+1) = key
    end do
end subroutine insertion_sort