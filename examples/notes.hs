( FunctionCall -1 "Logical 4 []_GtE"
                                        [
                                            ( Var 2 "i" )
                                        ,
                                            ( Constant
                                                ( DefinableType Just 4 "Integer" [] ) "0"
                                            )
                                        ] []
                                        ( DefinableType Just 4 "Logical" [] ) Nothing Nothing
                                    )
--Make restriction, Done!
--Sig ([T], [Func "GtE" [T,T] Logical])

--TODO: Make abstract derived type(in FORTRAN), investigate the ASR/XASR, check if we can use it to represent restrictions in ASR. --Can be done, Used to be done like that before.
--TODO: Instantiating, Done!
--TODO: Make sort for some intrinsic types, Real, Char-String(?), Done
--TODO: Modify XASR TranslationUnit with Intrinsic Symbol Table, Done!
--TODO: Send Email, Done!
--TODO: Discuss combining intrinsic signatures into one.??