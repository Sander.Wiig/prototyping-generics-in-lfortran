module TSortXASR where
import qualified ASR
import XASR

tsort::XASR
tsort=(TranslationUnit
        ( SymbolTable ( Nothing ) 1
            [
                ( "insertion_sort"
                ,
                    ( Subroutine
                        ( SymbolTable
                            ( Just 1 ) 2
                            [
                                ( "array"
                                ,
                                    ( Variable 2 "array" InOut Nothing Nothing Default
                                        ( DefinableType Nothing "T"
                                            [
                                                ( Dimension Just
                                                    ( Constant
                                                        ( DefinableType Just 4 "Integer" [] ) "0"
                                                    ) Nothing
                                                )
                                            ]
                                        ) Source Public Required False
                                    )
                                )
                            ,
                                ( "i"
                                ,
                                    ( Variable 2 "i" Local Nothing Nothing Default
                                        ( DefinableType Just 4 "Integer" [] ) Source Public Required False
                                    )
                                )
                            ,
                                ( "j"
                                ,
                                    ( Variable 2 "j" Local Nothing Nothing Default
                                        ( DefinableType Just 4 "Integer" [] ) Source Public Required False
                                    )
                                )
                            ,
                                ( "key"
                                ,
                                    ( Variable 2 "key" Local Nothing Nothing Default
                                        ( DefinableType Nothing "T" [] ) Source Public Required False
                                    )
                                )
                            ,
                                ( "size"
                                ,
                                    ( ExternalSymbol 2 "size" 4 "size" [] "size" Private )
                                )
                            ]
                        ) "insertion_sort"
                        [
                            ( Var 2 "array" )
                        ]
                        [
                            ( DoLoop
                                (
                                    ( Var 2 "j" )
                                ,
                                    ( Constant
                                        ( DefinableType Just 4 "Integer" [] ) "0"
                                    )
                                ,
                                    ( FunctionCall -1 "Integer_Sub"
                                        [
                                            ( FunctionCall 2 "size"
                                                [
                                                    ( Var 2 "array" )
                                                ,
                                                    ( Constant
                                                        ( DefinableType Just 4 "Integer" [] ) "0"
                                                    )
                                                ] []
                                                ( DefinableType Just 4 "Integer" [] ) Nothing Nothing
                                            )
                                        ,
                                            ( Constant
                                                ( DefinableType Just 4 "Integer" [] ) "1"
                                            )
                                        ] []
                                        ( DefinableType Just 4 "Integer" [] ) Nothing Nothing
                                    )
                                , Nothing
                                )
                                [
                                    ( Assignment
                                        ( Var 2 "key" )
                                        ( ArrayRef 2 "array"
                                            [
                                                ( Nothing
                                                , Just
                                                    ( Var 2 "j" )
                                                , Nothing
                                                )
                                            ]
                                            ( DefinableType Nothing "T"
                                                [
                                                    ( Dimension Just
                                                        ( Constant
                                                            ( DefinableType Just 4 "Integer" [] ) "0"
                                                        ) Nothing
                                                    )
                                                ]
                                            ) Nothing
                                        ) Nothing
                                    )
                                ,
                                    ( Assignment
                                        ( Var 2 "i" )
                                        ( FunctionCall -1 "Integer_Sub"
                                            [
                                                ( Var 2 "j" )
                                            ,
                                                ( Constant
                                                    ( DefinableType Just 4 "Integer" [] ) "1"
                                                )
                                            ] []
                                            ( DefinableType Just 4 "Integer" [] ) Nothing Nothing
                                        ) Nothing
                                    )
                                ,
                                    ( While
                                        ( FunctionCall -1 "Logical_GtE"
                                            [
                                                ( Var 2 "i" )
                                            ,
                                                ( Constant
                                                    ( DefinableType Just 4 "Integer" [] ) "0"
                                                )
                                            ] []
                                            ( DefinableType Just 4 "Logical" [] ) Nothing Nothing
                                        )
                                        [
                                            ( If
                                                ( FunctionCall -1 "T_Eq"
                                                    [
                                                        ( ArrayRef 2 "array"
                                                            [
                                                                ( Nothing
                                                                , Just
                                                                    ( Var 2 "i" )
                                                                , Nothing
                                                                )
                                                            ]
                                                            ( DefinableType Nothing "T"
                                                                [
                                                                    ( Dimension Just
                                                                        ( Constant
                                                                            ( DefinableType Just 4 "Integer" [] ) "0"
                                                                        ) Nothing
                                                                    )
                                                                ]
                                                            ) Nothing
                                                        )
                                                    ,
                                                        ( Var 2 "key" )
                                                    ] []
                                                    ( DefinableType Just 4 "Logical" [] ) Nothing Nothing
                                                ) [ ( Exit ) ] []
                                            )
                                        ,
                                            ( Assignment
                                                ( ArrayRef 2 "array"
                                                    [
                                                        ( Nothing
                                                        , Just
                                                            ( FunctionCall -1 "Integer_Add"
                                                                [
                                                                    ( Var 2 "i" )
                                                                ,
                                                                    ( Constant
                                                                        ( DefinableType Just 4 "Integer" [] ) "1"
                                                                    )
                                                                ] []
                                                                ( DefinableType Just 4 "Integer" [] ) Nothing Nothing
                                                            )
                                                        , Nothing
                                                        )
                                                    ]
                                                    ( DefinableType Nothing "T"
                                                        [
                                                            ( Dimension Just
                                                                ( Constant
                                                                    ( DefinableType Just 4 "Integer" [] ) "0"
                                                                ) Nothing
                                                            )
                                                        ]
                                                    ) Nothing
                                                )
                                                ( ArrayRef 2 "array"
                                                    [
                                                        ( Nothing
                                                        , Just
                                                            ( Var 2 "i" )
                                                        , Nothing
                                                        )
                                                    ]
                                                    ( DefinableType Nothing "T"
                                                        [
                                                            ( Dimension Just
                                                                ( Constant
                                                                    ( DefinableType Just 4 "Integer" [] ) "0"
                                                                ) Nothing
                                                            )
                                                        ]
                                                    ) Nothing
                                                ) Nothing
                                            )
                                        ,
                                            ( Assignment
                                                ( Var 2 "i" )
                                                ( FunctionCall -1 "Integer_Sub"
                                                    [
                                                        ( Var 2 "i" )
                                                    ,
                                                        ( Constant
                                                            ( DefinableType Just 4 "Integer" [] ) "1"
                                                        )
                                                    ] []
                                                    ( DefinableType Just 4 "Integer" [] ) Nothing Nothing
                                                ) Nothing
                                            )
                                        ]
                                    )
                                ,
                                    ( Assignment
                                        ( ArrayRef 2 "array"
                                            [
                                                ( Nothing
                                                , Just
                                                    ( FunctionCall -1 "Integer_Add"
                                                        [
                                                            ( Var 2 "i" )
                                                        ,
                                                            ( Constant
                                                                ( DefinableType Just 4 "Integer" [] ) "1"
                                                            )
                                                        ] []
                                                        ( DefinableType Just 4 "Integer" [] ) Nothing Nothing
                                                    )
                                                , Nothing
                                                )
                                            ]
                                            ( DefinableType Nothing "T"
                                                [
                                                    ( Dimension Just
                                                        ( Constant
                                                            ( DefinableType Just 4 "Integer" [] ) "0"
                                                        ) Nothing
                                                    )
                                                ]
                                            ) Nothing
                                        )
                                        ( Var 2 "key" ) Nothing
                                    )
                                ]
                            )
                        ] Source Public Implementation Nothing False
                    )
                )
            ,
                ( "Intrinsics"
                ,
                    ( Module
                        ( SymbolTable ( Nothing ) 3 [] ) "Intrinsics" [] True
                    )
                )
            ,
                ( "T"
                ,
                    ( Module
                        ( 
                            SymbolTable ( Nothing ) 4 [("T", Signature 0 ["T"] "T" 
                                [
                                    Function (
                                        SymbolTable (Just parent) addID [
                                                                            ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just 0) "T" "T" []) Intrinsic Public Required False),
                                                                            ("right", Variable addID "right" In Nothing Nothing Parameter (DefinableType (Just 0) "T" "T" []) Intrinsic Public Required False),
                                                                            ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just 0) "T" "T" []) Intrinsic Public Required False)
                                                                        ]
                                            )
                                    "T_Eq" [Var 4 "left", Var 4 "right" ] [] (Var 4 "return") Source Public Interface True Nothing
                                ]
                            )]
                        ) "T" [] True
                    )
                )
            ]
        )
    )
realSort::XASR
realSort = ( TranslationUnit
    ( SymbolTable ( Nothing ) 1
        [
            ( "insertion_sort"
            ,
                ( Subroutine
                    ( SymbolTable
                        ( Just 1 ) 2
                        [
                            ( "array"
                            ,
                                ( Variable 2 "array" InOut Nothing Nothing Default
                                    ( DefinableType Just 0 "Real"
                                        [
                                            ( Dimension Just
                                                ( Constant
                                                    ( DefinableType Just 4 "Integer" [] ) "0"
                                                ) Nothing
                                            )
                                        ]
                                    ) Source Public Required False
                                )
                            )
                        ,
                            ( "i"
                            ,
                                ( Variable 2 "i" Local Nothing Nothing Default
                                    ( DefinableType Just 4 "Integer" [] ) Source Public Required False
                                )
                            )
                        ,
                            ( "j"
                            ,
                                ( Variable 2 "j" Local Nothing Nothing Default
                                    ( DefinableType Just 4 "Integer" [] ) Source Public Required False
                                )
                            )
                        ,
                            ( "key"
                            ,
                                ( Variable 2 "key" Local Nothing Nothing Default
                                    ( DefinableType Just 0 "Real" [] ) Source Public Required False
                                )
                            )
                        ,
                            ( "size"
                            ,
                                ( ExternalSymbol 2 "size" 4 "size" [] "size" Private )
                            )
                        ]
                    ) "insertion_sort"
                    [
                        ( Var 2 "array" )
                    ]
                    [
                        ( DoLoop
                            (
                                ( Var 2 "j" )
                            ,
                                ( Constant
                                    ( DefinableType Just 4 "Integer" [] ) "0"
                                )
                            ,
                                ( FunctionCall -1 "Integer_Sub"
                                    [
                                        ( FunctionCall 2 "size"
                                            [
                                                ( Var 2 "array" )
                                            ,
                                                ( Constant
                                                    ( DefinableType Just 4 "Integer" [] ) "0"
                                                )
                                            ] []
                                            ( DefinableType Just 4 "Integer" [] ) Nothing Nothing
                                        )
                                    ,
                                        ( Constant
                                            ( DefinableType Just 4 "Integer" [] ) "1"
                                        )
                                    ] []
                                    ( DefinableType Just 4 "Integer" [] ) Nothing Nothing
                                )
                            , Nothing
                            )
                            [
                                ( Assignment
                                    ( Var 2 "key" )
                                    ( ArrayRef 2 "array"
                                        [
                                            ( Nothing
                                            , Just
                                                ( Var 2 "j" )
                                            , Nothing
                                            )
                                        ]
                                        ( DefinableType Nothing "T"
                                            [
                                                ( Dimension Just
                                                    ( Constant
                                                        ( DefinableType Just 4 "Integer" [] ) "0"
                                                    ) Nothing
                                                )
                                            ]
                                        ) Nothing
                                    ) Nothing
                                )
                            ,
                                ( Assignment
                                    ( Var 2 "i" )
                                    ( FunctionCall -1 "Integer_Sub"
                                        [
                                            ( Var 2 "j" )
                                        ,
                                            ( Constant
                                                ( DefinableType Just 4 "Integer" [] ) "1"
                                            )
                                        ] []
                                        ( DefinableType Just 4 "Integer" [] ) Nothing Nothing
                                    ) Nothing
                                )
                            ,
                                ( While
                                    ( FunctionCall -1 "Logical_GtE"
                                        [
                                            ( Var 2 "i" )
                                        ,
                                            ( Constant
                                                ( DefinableType Just 4 "Integer" [] ) "0"
                                            )
                                        ] []
                                        ( DefinableType Just 4 "Logical" [] ) Nothing Nothing
                                    )
                                    [
                                        ( If
                                            ( FunctionCall -1 "Real_Eq"
                                                [
                                                    ( ArrayRef 2 "array"
                                                        [
                                                            ( Nothing
                                                            , Just
                                                                ( Var 2 "i" )
                                                            , Nothing
                                                            )
                                                        ]
                                                        ( DefinableType Just 0 "Real"
                                                            [
                                                                ( Dimension Just
                                                                    ( Constant
                                                                        ( DefinableType Just 4 "Integer" [] ) "0"
                                                                    ) Nothing
                                                                )
                                                            ]
                                                        ) Nothing
                                                    )
                                                ,
                                                    ( Var 2 "key" )
                                                ] []
                                                ( DefinableType Just 4 "Logical" [] ) Nothing Nothing
                                            ) [ ( Exit ) ] []
                                        )
                                    ,
                                        ( Assignment
                                            ( ArrayRef 2 "array"
                                                [
                                                    ( Nothing
                                                    , Just
                                                        ( FunctionCall -1 "Integer_Add"
                                                            [
                                                                ( Var 2 "i" )
                                                            ,
                                                                ( Constant
                                                                    ( DefinableType Just 4 "Integer" [] ) "1"
                                                                )
                                                            ] []
                                                            ( DefinableType Just 4 "Integer" [] ) Nothing Nothing
                                                        )
                                                    , Nothing
                                                    )
                                                ]
                                                ( DefinableType Just 0 "Real"
                                                    [
                                                        ( Dimension Just
                                                            ( Constant
                                                                ( DefinableType Just 4 "Integer" [] ) "0"
                                                            ) Nothing
                                                        )
                                                    ]
                                                ) Nothing
                                            )
                                            ( ArrayRef 2 "array"
                                                [
                                                    ( Nothing
                                                    , Just
                                                        ( Var 2 "i" )
                                                    , Nothing
                                                    )
                                                ]
                                                ( DefinableType Just 0 "Real"
                                                    [
                                                        ( Dimension Just
                                                            ( Constant
                                                                ( DefinableType Just 4 "Integer" [] ) "0"
                                                            ) Nothing
                                                        )
                                                    ]
                                                ) Nothing
                                            ) Nothing
                                        )
                                    ,
                                        ( Assignment
                                            ( Var 2 "i" )
                                            ( FunctionCall -1 "Integer_Sub"
                                                [
                                                    ( Var 2 "i" )
                                                ,
                                                    ( Constant
                                                        ( DefinableType Just 4 "Integer" [] ) "1"
                                                    )
                                                ] []
                                                ( DefinableType Just 4 "Integer" [] ) Nothing Nothing
                                            ) Nothing
                                        )
                                    ]
                                )
                            ,
                                ( Assignment
                                    ( ArrayRef 2 "array"
                                        [
                                            ( Nothing
                                            , Just
                                                ( FunctionCall -1 "Integer_Add"
                                                    [
                                                        ( Var 2 "i" )
                                                    ,
                                                        ( Constant
                                                            ( DefinableType Just 4 "Integer" [] ) "1"
                                                        )
                                                    ] []
                                                    ( DefinableType Just 4 "Integer" [] ) Nothing Nothing
                                                )
                                            , Nothing
                                            )
                                        ]
                                        ( DefinableType Just 0 "Real"
                                            [
                                                ( Dimension Just
                                                    ( Constant
                                                        ( DefinableType Just 4 "Integer" [] ) "0"
                                                    ) Nothing
                                                )
                                            ]
                                        ) Nothing
                                    )
                                    ( Var 2 "key" ) Nothing
                                )
                            ]
                        )
                    ] Source Public Implementation Nothing False
                )
            )
        ,
            ( "Intrinsics"
            ,
                ( Module
                    ( SymbolTable ( Nothing ) 0 [] ) "Intrinsics" [] True
                )
            )
        ]
    )
)