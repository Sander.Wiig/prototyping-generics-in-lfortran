module SortASR where
import ASR
import Transform
import CommonDefinitions
import Text.Pretty.Simple (pPrint)

test::IO()
test = pPrint $ transform asr 

asr ::ASR.TranslationUnit
asr = 
TranslationUnit
    ( SymbolTable Nothing 1   
        [
            ( "insertion_sort"
            , Subroutine      
                ( SymbolTable 
                    ( Just 1 ) 2
                    [
                        ( "array"
                        , Variable 2 "array" InOut Nothing Nothing Default
                            ( Integer 4
                                [
                                    ( Just
                                        ( ConstantInteger 0
                                            ( Integer 4 [] )
                                        )
                                    , Nothing
                                    )
                                ]
                            ) Source Public Required False
                        )
                    ,
                        ( "i"
                        , Variable 2 "i" Local Nothing Nothing Default
                            ( Integer 4 [] ) Source Public Required False
                        )
                    ,
                        ( "j"
                        , Variable 2 "j" Local Nothing Nothing Default
                            ( Integer 4 [] ) Source Public Required False
                        )
                    ,
                        ( "key"
                        , Variable 2 "key" Local Nothing Nothing Default
                            ( Integer 4 [] ) Source Public Required False
                        )
                    ,
                        ( "size"
                        , ExternalSymbol 2 "size" 4 "size" "lfortran_intrinsic_builtin" [] "size" Private
                        )
                    ]
                ) "insertion_sort"
                [ Var 2 "array" ]
                [ DoLoop
                    ( Var 2 "j"
                    , ConstantInteger 0
                        ( Integer 4 [] )
                    , BinOp
                        ( FunctionCall 2 "size" Nothing
                            [ Var 2 "array"
                            , ConstantInteger 0
                                ( Integer 4 [] )
                            ] []
                            ( Integer 4 [] ) Nothing Nothing
                        ) Sub
                        ( ConstantInteger 1
                            ( Integer 4 [] )
                        )
                        ( Integer 4 [] ) Nothing Nothing
                    , Nothing
                    )
                    [ Assignment
                        ( Var 2 "key" )
                        ( ArrayRef 2 "array"
                            [
                                ( Nothing
                                , Just
                                    ( Var 2 "j" )
                                , Nothing
                                )
                            ]
                            ( Integer 4
                                [
                                    ( Just
                                        ( ConstantInteger 0
                                            ( Integer 4 [] )
                                        )
                                    , Nothing
                                    )
                                ]
                            ) Nothing
                        ) Nothing
                    , Assignment
                        ( Var 2 "i" )
                        ( BinOp
                            ( Var 2 "j" ) Sub
                            ( ConstantInteger 1
                                ( Integer 4 [] )
                            )
                            ( Integer 4 [] ) Nothing Nothing
                        ) Nothing
                    , WhileLoop
                        ( Compare
                            ( Var 2 "i" ) GtE
                            ( ConstantInteger 0
                                ( Integer 4 [] )
                            )
                            ( Logical 4 [] ) Nothing Nothing
                        )
                        [ If
                            ( Compare
                                ( ArrayRef 2 "array"
                                    [
                                        ( Nothing
                                        , Just
                                            ( Var 2 "i" )
                                        , Nothing
                                        )
                                    ]
                                    ( Integer 4
                                        [
                                            ( Just
                                                ( ConstantInteger 0
                                                    ( Integer 4 [] )
                                                )
                                            , Nothing
                                            )
                                        ]
                                    ) Nothing
                                ) Eq
                                ( Var 2 "key" )
                                ( Logical 4 [] ) Nothing Nothing
                            ) [ Exit ] []
                        , Assignment
                            ( ArrayRef 2 "array"
                                [
                                    ( Nothing
                                    , Just
                                        ( BinOp
                                            ( Var 2 "i" ) Add
                                            ( ConstantInteger 1
                                                ( Integer 4 [] )
                                            )
                                            ( Integer 4 [] ) Nothing Nothing
                                        )
                                    , Nothing
                                    )
                                ]
                                ( Integer 4
                                    [
                                        ( Just
                                            ( ConstantInteger 0
                                                ( Integer 4 [] )
                                            )
                                        , Nothing
                                        )
                                    ]
                                ) Nothing
                            )
                            ( ArrayRef 2 "array"
                                [
                                    ( Nothing
                                    , Just
                                        ( Var 2 "i" )
                                    , Nothing
                                    )
                                ]
                                ( Integer 4
                                    [
                                        ( Just
                                            ( ConstantInteger 0
                                                ( Integer 4 [] )
                                            )
                                        , Nothing
                                        )
                                    ]
                                ) Nothing
                            ) Nothing
                        , Assignment
                            ( Var 2 "i" )
                            ( BinOp
                                ( Var 2 "i" ) Sub
                                ( ConstantInteger 1
                                    ( Integer 4 [] )
                                )
                                ( Integer 4 [] ) Nothing Nothing
                            ) Nothing
                        ]
                    , Assignment
                        ( ArrayRef 2 "array"
                            [
                                ( Nothing
                                , Just
                                    ( BinOp
                                        ( Var 2 "i" ) Add
                                        ( ConstantInteger 1
                                            ( Integer 4 [] )
                                        )
                                        ( Integer 4 [] ) Nothing Nothing
                                    )
                                , Nothing
                                )
                            ]
                            ( Integer 4
                                [
                                    ( Just
                                        ( ConstantInteger 0
                                            ( Integer 4 [] )
                                        )
                                    , Nothing
                                    )
                                ]
                            ) Nothing
                        )
                        ( Var 2 "key" ) Nothing
                    ]
                ] Source Public Implementation Nothing True False
            )
        ]
    ) []