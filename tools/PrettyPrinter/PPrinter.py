from typing import List
from tokenize import String
from lark import Lark
from lark.visitors import Interpreter
import sys
sys.setrecursionlimit(100000)
#Program for generating hs show functions to make em print like S-Exprs
#see: https://xkcd.com/1319/

test="""(TranslationUnit (SymbolTable (Nothing) 1 [("insertion_sort",(Subroutine (SymbolTable (Just 1) 2 [("array",(Variable 2 "array" InOut Nothing Nothing Default (DefinableType Just 4 "Integer" [(Dimension Just (Constant (DefinableType Just 4 "Integer" []) "0") Nothing)]) Source Public Required False)),("i",(Variable 2 "i" Local Nothing Nothing Default (DefinableType Just 4 "Integer" []) Source Public Required False)),("j",(Variable 2 "j" Local Nothing Nothing Default (DefinableType Just 4 "Integer" []) Source Public Required False)),("key",(Variable 2 "key" Local Nothing Nothing Default (DefinableType Just 4 "Integer" []) Source Public Required False)),("size",(ExternalSymbol 2 "size" 4 "size" [] "size" Private))]) "insertion_sort" [(Var 2 "array")] [(DoLoop ((Var 2 "j"),(Constant (DefinableType Just 4 "Integer" []) "0"),(FunctionCall -1 "Integer 4 []_Sub" [(FunctionCall 2 "size" [(Var 2 "array"),(Constant (DefinableType Just 4 "Integer" []) "0")] [] (DefinableType Just 4 "Integer" []) Nothing Nothing),(Constant (DefinableType Just 4 "Integer" []) "1")] [] (DefinableType Just 4 "Integer" []) Nothing Nothing),Nothing) [(Assignment (Var 2 "key") (ArrayRef 2 "array" [(Nothing,Just (Var 2 "j"),Nothing)] (DefinableType Just 4 "Integer" [(Dimension Just (Constant (DefinableType Just 4 "Integer" []) "0") Nothing)]) Nothing) Nothing),(Assignment (Var 2 "i") (FunctionCall -1 "Integer 4 []_Sub" [(Var 2 "j"),(Constant (DefinableType Just 4 "Integer" []) "1")] [] (DefinableType Just 4 "Integer" []) Nothing Nothing) Nothing),(While (FunctionCall -1 "Logical 4 []_GtE" [(Var 2 "i"),(Constant (DefinableType Just 4 "Integer" []) "0")] [] (DefinableType Just 4 "Logical" []) Nothing Nothing) [(If (FunctionCall -1 "Logical 4 []_Eq" [(ArrayRef 2 "array" [(Nothing,Just (Var 2 "i"),Nothing)] (DefinableType Just 4 "Integer" [(Dimension Just (Constant (DefinableType Just 4 "Integer" []) "0") Nothing)]) Nothing),(Var 2 "key")] [] (DefinableType Just 4 "Logical" []) Nothing Nothing) [(Exit)] []),(Assignment (ArrayRef 2 "array" [(Nothing,Just (FunctionCall -1 "Integer 4 []_Add" [(Var 2 "i"),(Constant (DefinableType Just 4 "Integer" []) "1")] [] (DefinableType Just 4 "Integer" []) Nothing Nothing),Nothing)] (DefinableType Just 4 "Integer" [(Dimension Just (Constant (DefinableType Just 4 "Integer" []) "0") Nothing)]) Nothing) (ArrayRef 2 "array" [(Nothing,Just (Var 2 "i"),Nothing)] (DefinableType Just 4 "Integer" [(Dimension Just (Constant (DefinableType Just 4 "Integer" []) "0") Nothing)]) Nothing) Nothing),(Assignment (Var 2 "i") (FunctionCall -1 "Integer 4 []_Sub" [(Var 2 "i"),(Constant (DefinableType Just 4 "Integer" []) "1")] [] (DefinableType Just 4 "Integer" []) Nothing Nothing) Nothing)]),(Assignment (ArrayRef 2 "array" [(Nothing,Just (FunctionCall -1 "Integer 4 []_Add" [(Var 2 "i"),(Constant (DefinableType Just 4 "Integer" []) "1")] [] (DefinableType Just 4 "Integer" []) Nothing Nothing),Nothing)] (DefinableType Just 4 "Integer" [(Dimension Just (Constant (DefinableType Just 4 "Integer" []) "0") Nothing)]) Nothing) (Var 2 "key") Nothing)])] Source Public Implementation Nothing False))])"""

data_parser = Lark.open('s_expr.lark', rel_to=__file__, parser="earley",start='constructor')
tree =data_parser.parse(test)
class DataVisitor(Interpreter):
    tabs=0

    def constructor(self, tree):
        assert tree.data == "constructor"
        print(self.tabs*"    ("+str(tree.data.value))
        self.tabs+=1
        for child in tree.iter_subtrees():
            #print(child)
            DataVisitor().visit(child)
        print(self.tabs*"    "+")")
        self.tabs-=1

    def type(self, tree):
        assert tree.data == "type"
        print(self.tabs*"    "+str(tree.data.value))

    def list(self, tree):
        assert tree.data == "list"
        print(self.tabs*"    [\n")
        self.tabs+=1
        for child in tree.iter_subtrees():
            #print(child)
            DataVisitor().visit(child)
        print(self.tabs*"    "+"]\n")
        self.tabs-=1

    def tuple(self, tree):
        assert tree.data == "tuple"
        print("(\n")
        for child in tree.iter_subtrees():
            #print(child)
            DataVisitor().visit(child)
            print(",")
        print(")\n")

    def just(self, tree):
        assert tree.data == "just"
        if(tree == "Nothing"):
            print(self.tabs*"    Nothing\n")
        else:
            print(self.tabs*"    "+"Just "+str(tree.data.value))

    def SIGNED_NUMBER(self,tree):
        assert tree.data == "SIGNED_NUMBER"
        print(tree.data.value)

    def CNAME(self,tree):
        assert tree.data == "CNAME"
        print(tree.data.value)

    def STRING(self,tree):
        assert tree.data == "STRING"
        print(tree.data.value)

    def SYMBOL(self,tree):
        assert tree.data == "SYMBOL"
        print(tree.data.value)

    def __default__():
        pass
print(tree)
#DataVisitor().visit(tree)

    
