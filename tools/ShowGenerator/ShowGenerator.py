from typing import List
from tokenize import String
from lark import Lark, Visitor

#Program for generating hs show functions to make em print like S-Exprs
#see: https://xkcd.com/1319/

data_parser = Lark.open('grammar.lark', rel_to=__file__, parser="earley",start='file')

test = """
    data SymbolTable = SymbolTable (Maybe Int) Int [(Identifier, Symbol)] deriving (Show, Eq)

    """



class Constructor():
    name:String
    fields=[]

    def showConstructor(self)->String:
        str = "show ("+self.name
        for i in range(0.. len(self.fields)):
            str+=" v"+i
        str+=")="
        for i in range(0..len(self.fields)):
            str+= "(show v"+i+")"
        return str


class DataStruct():
    name:String
    cons=[]
    
    def showDataStruct(self):
        str="instance Show "+self.name+" where \n"
        for i in self.cons:
            str+=i.showConstructor()
            str+="\n"

class ProgramVisitor(Visitor):
    dataStructs=[]

    def constructor(self, constructor):
        c = Constructor()
        c.name = constructor.children[0].value
        ls =constructor.children
        ls.pop(0)
        for arg in ls:
            c.fields.append(self.constructorArg(arg))

    def constructorArg(self, arg):
        return arg.children[0].value

    def data(self, data):
        dta = DataStruct()
        dta.name = data.children[0].value
        
        ls=data.children
        ls.pop(0)
        for c in ls:
            dta.cons.append(self.constructor(c))
        return dta

    def file(self, datas):
        for dta in datas.children:
            self.dataStructs.append(self.data(dta))

tree =data_parser.parse(test)
print(tree.pretty())
print(ProgramVisitor().visit(tree))