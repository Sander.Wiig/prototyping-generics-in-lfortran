module Signature where
import qualified ASR
import XASR
import CommonDefinitions
import Examples.

makeSignature :: SymbolTable->SymbolName->Symbol
makeSignature (SymbolTable _ id _ ) name= XASR.Signature id name []

addFunctionToSignature::Program->SymbolName->([Expr],Expr)->Symbol->Symbol
addFunctionToSignature program funcName (params, return) (XASR.Signature id name members)
                = Function (newSymbolTable Nothing (getNextSymbolTableID program)) funcName params []
                    return Intrinsic Public Interface True Nothing

newSymbolTable:: Maybe Int->Int->SymbolTable
newSymbolTable parentId id  = SymbolTable parentId id []

