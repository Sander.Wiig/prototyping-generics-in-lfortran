{-# LANGUAGE DeriveAnyClass #-}
--Changes
--Got rid of most intrinsic types and folded them and their intrinsic ops into signature
--got rid of intrinsic op specific expressions and made them into functions
--ditto with cast
--Added Signature symbol
--Added PurityType which wil be usefull in checking if functions can operate on arrays
--Removed classes and stuff.
--Annotated all data structures.

module XASR where
import CommonDefinitions
import Data.Maybe

data TranslationUnit = TranslationUnit {global_scope::SymbolTable, items::[Node], intrinsic::SymbolTable} deriving (Eq)

type SymTabMap = [(Identifier, Symbol)]
type Program = SymbolTable

--ASDL extended types
data SymbolTable = SymbolTable {parent::Maybe Int, symtabid:: Int, symbol_map:: SymTabMap} deriving (Eq)--TODO: MAybe look at different ways of doing symtab, please god yes
data Node = Symbol Symbol | Stmt Stmt | Expr Expr deriving (Eq)

data Symbol
    = Program{symtab:: SymbolTable, name:: SymbolName, dependencies:: [Identifier], body:: [Stmt]}
    | Module{symtab::SymbolTable, name:: SymbolName, dependencies:: [Identifier], loaded_from_mod:: Bool}
    --Look at combining Subroutines and Function??
    | Subroutine{symtab:: SymbolTable, name:: SymbolName, params:: [Expr],body::[Stmt],
        abi::Abi, acess:: Access, deftype:: DefType, bindc_name::Maybe String, pure:: Bool, mod::Bool}
    | Function { symtab::SymbolTable, name:: SymbolName, args:: [Expr], body:: [Stmt],
        return::Expr, abi:: Abi, acess:: Access, deftype:: DefType,is_pure:: Bool,bindc_name:: Maybe String}
    | GenericProcedure{ parent_symtab:: ParentSymbolTable, name::SymbolName, procs:: [Int], access::Access}
    | CustomOperator {parent_symtab::ParentSymbolTable, name:: SymbolName, procs:: [Int], access:: Access}
    | ExternalSymbol {parent_symtab::ParentSymbolTable, name:: SymbolName, external_id::Int, external_name:: SymbolName,module_name:: Identifier,scope_names:: [Identifier], original_name:: Identifier,access:: Access}
    | DerivedType {symtab::SymbolTable, name:: SymbolName, members:: [Identifier],
        abi::Abi,access:: Access, parent_symbol:: Maybe Int}
    | Variable {parent_symtab::ParentSymbolTable,name::SymbolName, intent::Intent,symbolic_value:: Maybe Expr,value:: Maybe Expr, storageType::StorageType, ttype:: TType, abi:: Abi, access:: Access,presence:: Presence, value_attr:: Bool}
    | Signature {parent_symtab::ParentSymbolTable, declaredTypes::[TType], name:: SymbolName, operations:: [Symbol]}
    deriving (Eq)

data TType
    = DefinableType {kind::Maybe Int, signature::SymbolName} --TODO: Remove dimension
    | Pointer {pointer_type::TType}
    | Array {array_type::TType, dimension::[Dimension]}--TODO: Write dim to array transform
    deriving (Eq)

type Dimension = (Maybe Expr, Maybe Expr)

data PurityType = Pure | Simple | Elemental | Unpure deriving (Eq)

data Stmt
    = Allocate {alloc_args::[AllocArg],stat:: Maybe Expr}
    | Assign {label:: Int, variable ::Identifier}
    | Assignment {target::Expr, val:: Expr, overloaded::Maybe Stmt}
    | Associate {target::Expr, val:: Expr}
    | Cycle
    | Deallocate{vars:: [SymbolName]}
    | DoConcurrentLoop {loop_head::DoLoopHead, loop_body:: [Stmt]}
    | DoLoop {loop_head::DoLoopHead, loop_body:: [Stmt]}
    | ErrorStop {code::Maybe Expr}
    | Exit
    | ForAllSingle {loop_head::DoLoopHead, assign:: Stmt}
    | GoTo {target_id:: Int}
    | GoToTarget {id::Int}
    | If {test::Expr,if_body:: [Stmt], or_else:: [Stmt]}
    | IfArithmetic{test::Expr,lt_label::Int,eq_label:: Int,gt_label::Int}
    | Return
    | Select{test:: Expr, cases:: [CaseStmt], default_body:: [Stmt]}
    | Stop {code::Maybe Expr}
    | SubroutineCall{subroutine_id::Int,subroutine_name::SymbolName, original_name':: Maybe SymbolName, arguments:: [Expr], dt:: Maybe Expr}
    | Where {test::Expr, where_body:: [Stmt], or_else:: [Stmt]}
    | WhileLoop {test::Expr, while_body:: [Stmt]}
    | Nullify {vars::[SymbolName]}
    deriving (Eq)

type AllocArg = (SymbolName, [Dimension])

data Expr 
    = FunctionCall {symbol_ref::Int, function_name::SymbolName, orig_name:: Maybe SymbolName, args':: [Expr], key_words:: [Keyword], return_type:: TType, value':: Maybe Expr, dt':: Maybe Expr}
    | ImpliedDoLoop {values::[Expr], var::Expr, start':: Expr,end:: Expr, increment:: Maybe Expr, loop_type:: TType, value':: Maybe Expr}
    | DerivedTypeConstructor {symbol_ref::Int, dt_symbol::SymbolName, args'::[Expr], derivedType:: TType} --Generalize Constants into derived type constructor
    | Var {symbol_ref::Int, symbol::SymbolName}
    | ArrayRef {array_ref::Int,array_symbol::SymbolName, array_args:: [ArrayIndex], arrayType::TType,value'::Maybe Expr}
    | DerivedRef {v::Expr,m:: Symbol,refType::TType, value':: Maybe Expr}
    | Constant {tp::TType, data_str::String}
    | ConstantArray {elements::[Expr], arrayType::TType}
    deriving (Eq)

type Keyword = (Maybe Identifier{-arg-}, Expr{-value-})

type ArrayIndex = (Maybe Expr{-left-}, Maybe Expr{-right-}, Maybe Expr{-step-})

type DoLoopHead = (Expr{-v-}, Expr{-start-}, Expr{-end-}, Maybe Expr{-increment-})

data CaseStmt = CaseStmt [Expr]{-test-} [Stmt]{-body-} | CaseStmtRange (Maybe Expr){-start-} (Maybe Expr){-end-} [Stmt]{-body-} deriving (Eq)

lookupSymbolTable ::ParentSymbolTable->Program->Maybe SymbolTable 
lookupSymbolTable i (SymbolTable k n maps) = if i==n then Just (SymbolTable k n maps)
                                            else 
                                                let symTabs = map (lookupSymbolTable i .getSymbolTable.snd) maps 
                                                in if all isNothing symTabs then Nothing 
                                                    else
                                                        head $ filter isJust symTabs

getAllVisibleSymbols ::SymbolTable->Program->[(Identifier, Symbol)]                            
getAllVisibleSymbols start@(SymbolTable (Just k) n maps) program = case lookupSymbolTable k program of
                                                            Just s -> maps++ getAllVisibleSymbols s program
                                                            Nothing-> maps
getAllVisibleSymbols start@(SymbolTable Nothing n maps) program = maps

getSymbolTable::Symbol -> SymbolTable
getSymbolTable (Program s _ _ _) = s 
getSymbolTable (Module s _ _ _) =s
getSymbolTable (Subroutine s _ _ _ _ _ _ _ _ _)=s
getSymbolTable (Function s _ _ _ _ _ _ _ _ _) =s
getSymbolTable _=emptySymbolTable

emptySymbolTable :: SymbolTable 
emptySymbolTable = SymbolTable Nothing 0 []

getAllSymbolsInTable ::SymbolTable ->[(Identifier, Symbol)]
getAllSymbolsInTable (SymbolTable _ _ maps) = maps ++ concatMap (getAllSymbolsInTable . getSymbolTable . snd) maps

getNextSymbolTableID::Program->Int
getNextSymbolTableID (SymbolTable _ id maps) = foldr1
                                            (\ x y -> if x >= y then x else y)
                                            (id : map (getNextSymbolTableID . getSymbolTable . snd) maps)
                                            + 1

instance Show TranslationUnit where
    show (TranslationUnit program nodes intrinsics) = "(TranslationUnit "++ show program ++ concatMap show nodes ++ " "++ show intrinsics++")"
instance Show SymbolTable where
    show (SymbolTable (Just parentSymTab) id map) = "(SymbolTable "++ "(Just "++show parentSymTab ++") "++show id ++" " ++show map++")"
    show (SymbolTable Nothing  id map) = "(SymbolTable (Nothing) "++show id++" " ++ show map++")"
instance Show Node where
    show (Symbol sym) = "(Symbol "++show sym++")"
    show (Stmt stmt) = "(Stmt "++show stmt++")"
    show (Expr expr) = "(Expr "++show expr++")"
instance Show Symbol where 
    show (Program symTab name dependencies stmts) = "(Program "++ show symTab++" " ++ name++" " ++ show dependencies++" "++ show dependencies++" " ++ show stmts++")"
    show (Module symTab name dependencies bool) = "(Module "++show symTab ++" "++ name ++" " ++show dependencies ++ " "++ show bool++")"
    show (Subroutine symTab name args body abi access deftype bool isPure bindcname) = "(Subroutine "++ show symTab ++" "++show name++" " ++ show args ++ " "++show body ++ " "++show abi ++ " "++show access ++ " "++ show deftype ++ " " ++show bool ++ " "++show bindcname++")"
    show (Function symTab name args body return abi access deftype isPure bindcname) = "(Function "++ show symTab ++" "++show name++" " ++ show args ++ " "++show body ++ " "++show return ++" " ++show abi ++ " "++show access ++ " "++ show deftype ++ " "++ show bindcname++")"
    show (GenericProcedure parSymTab name procs access) = "(GenericProcedure "++show parSymTab++" "++ show name ++" "++show procs ++ " "++ show access ++")"
    show (ExternalSymbol parSymTab name id external module_name scope_names original_name access) = "(ExternalSymbol "++show parSymTab ++ " "++show name ++" "++show id ++ " "++show external ++ " "++ show scope_names ++ " "++ show original_name ++ " "++show access ++ ")"
    show (DerivedType symTab name members abi access parent)= "(DerivedType "++show symTab ++ " "++ show name ++ " "++show members ++ " "++show abi ++ " "++ show access ++ " " ++ show parent ++")"
    show (Variable parSymTab name intent symbolic_value value storage_type ttype abi access presence value_attr) = "(Variable "++show parSymTab ++ " "++show name++" "++show intent ++ " "++show symbolic_value ++ " "++ show value ++ " "++show storage_type ++ " " ++ show ttype ++ " "++show abi ++ " "++ show access ++ " "++ show presence ++ " "++show value_attr++")"
    show (Signature parSymTab types name members) = "(Signature "++show parSymTab ++" " ++ show types ++" "++show name ++ " "++show members++ ")"
instance Show TType where
    show (DefinableType kind signature) = "(DefinableType "++show kind ++ " "++show signature ++ ")"
    show (Pointer ttype) = "(Pointer "++show ttype ++")"
    show (Array ttype dims) = "(Array "++show ttype ++ " "++ show dims ++ ")"
instance Show Stmt where
    show (Allocate args stat) = "(Allocate "++ show args ++ " "++ show stat ++")"
    show (Assign label variable) = "(Assign "++show label ++ " "++ show variable ++ ")"
    show (Assignment target val overloaded) = "(Assignment "++show target ++ " "++show val ++" "++ show overloaded ++ ")"
    show (Associate target val) = "(Associate "++show target ++ " "++show val ++ ")"
    show (Cycle) = "(Cycle)"
    show (Deallocate vars) = "(Deallocate "++show vars ++ ")"
    show (DoConcurrentLoop head body) = "(DoConcurrentLoop "++show head ++" "++show body ++")" 
    show (DoLoop head body) = "(DoLoop "++show head ++" "++show body ++")" 
    show (ErrorStop code) = "(ErrorStop "++show code ++")"
    show (Exit) = "(Exit)"
    show (ForAllSingle head assign) = "(ForAllSingle " ++ show head ++ " "++show assign ++ ")"
    show (GoTo label) = "(GoTo "++show label ++")"
    show (GoToTarget label) = "(GoToTarget "++show label ++ ")"
    show (If test body body') = "(If "++show test ++" "++ show body ++" "++show body'++")"
    show (IfArithmetic test lt eq gt) = "(IfArithmetic "++show test ++ " "++show lt ++ " "++show eq ++" "++ show gt++")"
    show (Return) = "(Return)"
    show (Select test body def) = "(Select "++show test ++ " "++ show body ++ " "++show def ++")"
    show (Stop code) = "(Show "++show code++")"
    show (SubroutineCall id name original_name args dt) = "(SubroutineCall "++show id++" "++show name ++ " "++show original_name++" "++show args ++" "++show dt++")"
    show (Where test body or_else) = "(Where "++show test ++" "++show body ++ " "++show or_else ++")"
    show (WhileLoop test body) = "(While "++show test ++ " "++ show body ++ ")"
    show (Nullify vars) = "(Nullify " ++show vars ++ ")"

instance Show Expr where
    show (FunctionCall id name original_name args keywords ttype value dt) = "(FunctionCall "++show id++" "++show name ++ " "++show args ++ " "++show keywords ++ " "++ show ttype ++" "++show value++ " "++ show dt ++ ")"
    show (ImpliedDoLoop values var start stop increment ttype value) = "(ImpliedDoLoop "++show values ++ " "++ show var++ " "++ show start++" "++show stop ++" "++show increment ++ " "++ show ttype ++ " "++ show value ++ ")"
    show (DerivedTypeConstructor id dt args ttype) ="(DerivedTypeConstructor " ++show id++" "++ show dt++" "++ show args++" "++ show ttype++")"
    show (Var id symid) = "(Var "++show id ++" "++show symid ++ ")"
    show (ArrayRef id sym args ttype value)= "(ArrayRef "++show id++" "++show sym ++" " ++ show args++" " ++show ttype++" "++show value++")"
    show (DerivedRef v m ttype value) = "(DerivedRef "++show v ++" " ++ show m ++" " ++ show ttype ++" " ++ show value ++")"
    show (Constant ttype dta) = "(Constant "++ show ttype ++" " ++show dta ++")"
instance Show CaseStmt where
    show (CaseStmt tests bodies) = "(CaseStmt "++show tests ++ " "++ show bodies ++")"  
    show (CaseStmtRange start end bodies) = "(CaseStmtRange "++show start ++ " "++show end ++ " "++show bodies ++ ")"
