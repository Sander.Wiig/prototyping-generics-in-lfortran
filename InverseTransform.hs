module InverseTransform where
import ASR
import qualified XASR
import CommonDefinitions
import qualified Control.Arrow as Data.Bifunctor
import Data.Maybe
import Data.List

inverseTransform::XASR.TranslationUnit ->ASR.TranslationUnit 
inverseTransform (XASR.TranslationUnit symtab nodes intrinsics) = ASR.TranslationUnit st (map transformNode nodes) where st = transformSymbolTable symtab

transformNode :: XASR.Node -> ASR.Node
transformNode (XASR.Symbol symbol) = ASR.Symbol (transformSymbol symbol)
transformNode (XASR.Stmt stmt) = ASR.Stmt (transformStmt stmt)
transformNode (XASR.Expr expr) = ASR.Expr (transformExpr expr)

transformSymbolTable :: XASR.SymbolTable -> ASR.SymbolTable 
transformSymbolTable (XASR.SymbolTable (Just symtab) id mp) = ASR.SymbolTable (Just symtab) id  (map (Data.Bifunctor.second transformSymbol) mp) 
transformSymbolTable (XASR.SymbolTable Nothing id mp) = ASR.SymbolTable Nothing id (map (Data.Bifunctor.second transformSymbol) mp) 

transformSymbol :: XASR.Symbol -> ASR.Symbol
transformSymbol (XASR.Program symtab id args body) = ASR.Program (transformSymbolTable symtab) id args (map transformStmt body)
transformSymbol (XASR.Module symbol_table name dependencies loaded_from_mod) = ASR.Module (transformSymbolTable symbol_table) name dependencies loaded_from_mod
transformSymbol (XASR.Subroutine symbol_table name args body abi access deftype bindc_name pure ismodule) = ASR.Subroutine (transformSymbolTable symbol_table) name (map transformExpr args) (map transformStmt body) abi access deftype bindc_name pure ismodule
transformSymbol (XASR.Function symbol_table name args body return_var abi access deftype pure bindc_name) = ASR.Function (transformSymbolTable symbol_table) name (map transformExpr args) (map transformStmt body) (transformExpr return_var) abi access deftype pure bindc_name
transformSymbol (XASR.GenericProcedure symbol_table name procs access) = ASR.GenericProcedure symbol_table name procs access
transformSymbol (XASR.CustomOperator symbol_table name procs access) = ASR.CustomOperator symbol_table name procs access
transformSymbol (XASR.ExternalSymbol id symboltable name external module_name scope_names original_name access) = ASR.ExternalSymbol id symboltable name external module_name scope_names original_name access
transformSymbol (XASR.DerivedType symbol_table name members abi access Nothing) = ASR.DerivedType (transformSymbolTable symbol_table) name members abi access Nothing
transformSymbol (XASR.DerivedType symbol_table name members abi access (Just symbol)) = ASR.DerivedType (transformSymbolTable symbol_table) name members abi access (Just symbol)
transformSymbol (XASR.Variable parent_symtab name intent symbolic_val value storage ttype abi access presence value_attr) = ASR.Variable parent_symtab name intent (transformMaybeExpr symbolic_val) (transformMaybeExpr value) storage (transformTType ttype) abi access presence value_attr
transformSymbol (XASR.Signature symtab types id members) = undefined--ASR.DerivedType ASR.emptySymbolTable id members Intrinsic Public Nothing

transformStmt :: XASR.Stmt -> ASR.Stmt
transformStmt (XASR.Allocate args stat) = ASR.Allocate (map transformAllocArg args) (transformMaybeExpr stat)
transformStmt (XASR.Assign label variable) = ASR.Assign label variable 
transformStmt (XASR.Assignment target value (Just overloaded)) = ASR.Assignment (transformExpr target) (transformExpr value) (Just (transformStmt overloaded))
transformStmt (XASR.Assignment target value Nothing) = ASR.Assignment (transformExpr target) (transformExpr value) Nothing
transformStmt (XASR.Associate target value) = ASR.Associate (transformExpr target) (transformExpr value)
transformStmt XASR.Cycle = ASR.Cycle 
transformStmt (XASR.Deallocate vars) =ASR.ExplicitDeallocate vars
transformStmt (XASR.DoConcurrentLoop head body) = ASR.DoConcurrentLoop (transformDoLoopHead head) (map transformStmt body)
transformStmt (XASR.DoLoop head body) = ASR.DoLoop (transformDoLoopHead head) (map transformStmt body)
transformStmt (XASR.ErrorStop (Just expr)) = ASR.ErrorStop (Just (transformExpr expr))
transformStmt (XASR.ErrorStop Nothing) = ASR.ErrorStop Nothing
transformStmt XASR.Exit = ASR.Exit 
transformStmt (XASR.ForAllSingle head assign_stmt) = ASR.ForAllSingle (transformDoLoopHead head) (transformStmt assign_stmt)
transformStmt (XASR.GoTo target_id) = ASR.GoTo target_id
transformStmt (XASR.GoToTarget id) = ASR.GoToTarget id
transformStmt (XASR.If test stmts orelse) = ASR.If (transformExpr test) (map transformStmt stmts) (map transformStmt orelse)
transformStmt (XASR.IfArithmetic test lt_label eq_label gt_label)=ASR.IfArithmetic (transformExpr test) lt_label eq_label gt_label
transformStmt XASR.Return = ASR.Return
transformStmt (XASR.Select test body def) = ASR.Select (transformExpr test) (map transformCaseStmt body) (map transformStmt def)
transformStmt (XASR.Stop code) = ASR.Stop (transformMaybeExpr code)
transformStmt (XASR.SubroutineCall id name original_name args dt) = ASR.SubroutineCall id name original_name (map transformExpr args) (transformMaybeExpr dt)
transformStmt (XASR.Where test body orelse) = ASR.Where (transformExpr test) (map transformStmt body) (map transformStmt orelse)
transformStmt (XASR.WhileLoop test body) = ASR.WhileLoop (transformExpr test) (map transformStmt body)

transformExpr ::XASR.Expr ->ASR.Expr
transformExpr (XASR.FunctionCall id name original_name args keywords tp value dt) = case getType name of
                                                                                    "Integer"-> makeExpr args name tp
                                                                                    "Real"-> makeExpr args name tp
                                                                                    "Complex"-> makeExpr args name tp
                                                                                    "Logical"-> makeExpr args name tp
                                                                                    otherwise->ASR.FunctionCall id name original_name (map transformExpr args) (map transformKeyword keywords) (transformTType tp) (transformMaybeExpr value) (transformMaybeExpr dt) 



transformExpr (XASR.ImpliedDoLoop values var start end increment tp value) = ASR.ImpliedDoLoop (map transformExpr values) (transformExpr var) (transformExpr start) (transformExpr end) (transformMaybeExpr increment) (transformTType tp) (transformMaybeExpr value)
transformExpr (XASR.DerivedTypeConstructor dt_id dt_symb args tp) = ASR.DerivedTypeConstructor dt_id dt_symb (map transformExpr args) (transformTType tp) 
transformExpr (XASR.Var id v) = ASR.Var id v
transformExpr (XASR.ArrayRef i sym args tp val) = ASR.ArrayRef i sym (map transformArrayIndex args) (transformTType tp) (transformMaybeExpr val)
transformExpr (XASR.DerivedRef v m tp value) = undefined --TODO: May need discussion about this

transformExpr (XASR.Constant sym dta)= case sym of
                            a@(XASR.DefinableType (Just k) s)  | s=="Integer" -> ASR.ConstantInteger (read dta) (transformTType a)
                                                             | s=="Real" -> ASR.ConstantReal (read dta) (transformTType a)
                                                             | s=="Logical"-> ASR.ConstantLogical (read dta) (transformTType a)
                                                             | s=="Complex"-> ASR.ConstantComplex (readRe dta) (readIm dta) (transformTType a)
                                                             | s=="Character"->undefined
                            _ -> error "Not a constant/No kind"


makeExpr::[XASR.Expr]->String->XASR.TType->ASR.Expr
makeExpr args name tp = let argTp = findOpType . tail $ dropWhile (/='_') name in case argTp of
                        Bool -> ASR.BoolOp (transformExpr (args!!0)) (getBoolOp name) (transformExpr (args!!1)) (transformTType tp) Nothing
                        Bin -> ASR.BinOp (transformExpr (args!!0)) (getBinOp name) (transformExpr (args!!1)) (transformTType tp) Nothing Nothing
                        Un -> ASR.UnaryOp (getUnOp name) (transformExpr (args!!0)) (transformTType tp) Nothing
                        Str -> ASR.StrOp (transformExpr (args!!0)) (getStrOp name) (transformExpr (args!!1)) (transformTType tp) Nothing
                        Cmp -> ASR.Compare (transformExpr (args!!0)) (getCmpOp name) (transformExpr (args!!1)) (transformTType tp) Nothing Nothing

data OpType = Bool | Bin | Un | Str | Cmp |Other

findOpType::String->OpType
findOpType op | isInfixOf "And"op=Bool
            | isInfixOf "Or"op=Bool
            | isInfixOf "Xor"op=Bool
            | isInfixOf "NEqv"op=Bool
            | isInfixOf "Eqv"op=Bool

            | isInfixOf "Sub"op=Bin
            | isInfixOf "Add"op=Bin
            | isInfixOf "Mul"op=Bin
            | isInfixOf "Div"op=Bin
            | isInfixOf "Pow"op=Bin

            | isInfixOf "Invert"op=Un
            | isInfixOf "Not"op=Un
            | isInfixOf "UAdd"op=Un
            | isInfixOf "USub"op=Un

            | isInfixOf "Concat"op=Str
            | isInfixOf "Repeat"op=Str
        
            | isInfixOf "Eq"op=Cmp
            | isInfixOf "NotEq"op=Cmp
            | isInfixOf "Lt"op=Cmp
            | isInfixOf "LtE"op=Cmp
            | isInfixOf "Gt"op=Cmp
            | isInfixOf "GtE"op=Cmp

getType::String->String
getType = takeWhile (/='_')

getBoolOp::String->Boolop
getBoolOp op  | isInfixOf "And" op=And
            | isInfixOf "Or" op=Or
            | isInfixOf "Xor"op=Xor
            | isInfixOf "NEqv"op=NEqv
            | isInfixOf "Eqv"op=Eqv

getBinOp::String->Binop
getBinOp op   | isInfixOf "Sub" op=Sub
            | isInfixOf "Add"op=Add
            | isInfixOf "Mul"op=Mul
            | isInfixOf "Div"op=Div
            | isInfixOf "Pow"op=Pow
getUnOp::String->Unaryop
getUnOp op     | isInfixOf "Invert" op=Invert
            | isInfixOf "Not" op=Not
            | isInfixOf "UAdd" op=UAdd
            | isInfixOf "USub" op=USub

getStrOp::String->Strop
getStrOp op   | isInfixOf "Concat" op=Concat
            | isInfixOf "Repeat" op=Repeat
getCmpOp::String->Cmpop
getCmpOp op   | isInfixOf "Eq" op =Eq
            | isInfixOf "NotEq" op=NotEq
            | isInfixOf "Lt" op=Lt
            | isInfixOf "LtE" op=LtE
            | isInfixOf "Gt" op=Gt
            | isInfixOf "GtE" op=GtE
  
     

readRe::String->Float
readRe = read . takeWhile (/= ':')
readIm::String->Float
readIm = read . tail . dropWhile ( /= ':')

transformMaybeExpr :: Maybe XASR.Expr -> Maybe ASR.Expr 
transformMaybeExpr Nothing = Nothing 
transformMaybeExpr (Just expr) = Just (transformExpr expr)

transformCaseStmt ::XASR.CaseStmt ->ASR.CaseStmt
transformCaseStmt (XASR.CaseStmt xs ss) = ASR.CaseStmt (map transformExpr xs) (map transformStmt ss)
transformCaseStmt (XASR.CaseStmtRange e e1 ss) = ASR.CaseStmtRange (transformMaybeExpr e) (transformMaybeExpr e1) (map transformStmt ss)

transformAllocArg :: XASR.AllocArg ->ASR.AllocArg
transformAllocArg (symbol, dims) = (symbol, map transformDimension dims)

transformDimension :: XASR.Dimension  ->ASR.Dimension 
transformDimension (exprs, exprs') = (transformMaybeExpr exprs, transformMaybeExpr exprs')

transformDoLoopHead :: XASR.DoLoopHead ->ASR.DoLoopHead 
transformDoLoopHead (v, start, end, Just increment) = (transformExpr v, transformExpr start, transformExpr end, Just (transformExpr increment))
transformDoLoopHead (v, start, end, Nothing) = (transformExpr v, transformExpr start, transformExpr end, Nothing)

transformKeyword :: XASR.Keyword ->ASR.Keyword 
transformKeyword (id, expr) = (id, transformExpr expr)

transformArrayIndex ::XASR.ArrayIndex->ASR.ArrayIndex
transformArrayIndex (a,b,c) = (transformMaybeExpr a, transformMaybeExpr b, transformMaybeExpr c)

fromSignature:: XASR.Symbol -> ASR.TType
fromSignature (XASR.Signature _ _ "Integer" _) =  Integer 0 []
fromSignature (XASR.Signature _ _ "Real" _) = Real 0 []
fromSignature (XASR.Signature _ _ "Logical" _) = Logical 0 []
fromSignature (XASR.Signature _ _ "Complex" _) = Complex 0 []


transformTType ::XASR.TType ->ASR.TType 
transformTType (XASR.Pointer ttype )= ASR.Pointer $transformTType ttype
--Cant be this simple
transformTType (XASR.DefinableType kind "Integer") = case kind of 
                                                    Just k -> Integer k []
                                                    Nothing ->error "Missing kind"

transformTType (XASR.DefinableType kind "Real") = case kind of 
                                                    Just k -> Real k []
                                                    Nothing ->error "Missing kind"

transformTType (XASR.DefinableType kind "Complex") = case kind of 
                                                    Just k -> Complex k []
                                                    Nothing ->error "Missing kind"

transformTType (XASR.DefinableType kind "Character") = case kind of 
                                                    Just k -> Character k 1 Nothing []--TODO: Unsure about this
                                                    Nothing ->error "Missing kind"

transformTType (XASR.DefinableType kind "Logical") = case kind of 
                                                    Just k -> Logical k []
                                                    Nothing ->error "Missing kind"

transformTType (XASR.DefinableType kind sig) = Derived sig []

transformTType (XASR.Array tp@(XASR.DefinableType (Just k) sig) dims) = case sig of 
                                                                    "Logical"-> Logical k (map transformDimension dims)
                                                                    "Integer"->Integer k (map transformDimension dims)
                                                                    "Real"->Real k (map transformDimension dims)
                                                                    "Complex"->Complex k (map transformDimension dims)
                                                                    "Character"->Character k 1 Nothing (map transformDimension dims)
                                                                    otherwise -> Derived sig (map transformDimension dims)
transformTType (XASR.Array tp dims) = error "We do not support multi dimensional arrays at this moment"