module Intrinsics.Real where
import CommonDefinitions
import XASR
import qualified ASR

realSignature::ParentSymbolTable->Program->Symbol
realSignature parent program= Signature parent [DefinableType (Just parent) "Real", DefinableType (Just parent) "Logical"] "Real" [
                                                        --Add | Sub | Mul | Div | Pow
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just parent) "Real") Intrinsic Public Required False),
                                                                        ("right", Variable addID "right" In Nothing Nothing Parameter (DefinableType (Just parent) "Real") Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just parent) "Real") Intrinsic Public Required False)
                                                                     ]) "Real_Add"[
                                                                         Var addID "left",
                                                                         Var addID "Right"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just parent) "Real") Intrinsic Public Required False),
                                                                        ("right", Variable addID "right" In Nothing Nothing Parameter (DefinableType (Just parent) "Real") Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just parent) "Real") Intrinsic Public Required False)
                                                                     ]) "Real_Sub"[
                                                                         Var addID "left",
                                                                         Var addID "Right"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just parent) "Real" ) Intrinsic Public Required False),
                                                                        ("right", Variable addID "right" In Nothing Nothing Parameter (DefinableType (Just parent) "Real" ) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just parent) "Real" ) Intrinsic Public Required False)
                                                                     ]) "Real_Mul"[
                                                                         Var addID "left",
                                                                         Var addID "Right"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just parent) "Real" ) Intrinsic Public Required False),
                                                                        ("right", Variable addID "right" In Nothing Nothing Parameter (DefinableType (Just parent) "Real" ) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just parent) "Real" ) Intrinsic Public Required False)
                                                                     ]) "Real_Div"[
                                                                         Var addID "left",
                                                                         Var addID "Right"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just parent) "Real" ) Intrinsic Public Required False),
                                                                        ("right", Variable addID "right" In Nothing Nothing Parameter (DefinableType (Just parent) "Real" ) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just parent) "Real" ) Intrinsic Public Required False)
                                                                     ]) "Real_Pow"[
                                                                         Var addID "left",
                                                                         Var addID "Right"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        --Invert | Not | UAdd | USub
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just parent) "Real" ) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just parent) "Real" ) Intrinsic Public Required False)
                                                                     ]) "Real_UAdd"[
                                                                         Var addID "left"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just parent) "Real" ) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just parent) "Real" ) Intrinsic Public Required False)
                                                                     ]) "Real_USub"[
                                                                         Var addID "left"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        --Eq | NotEq | Lt | LtE | Gt | GtE
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just parent) "Real" ) Intrinsic Public Required False),
                                                                        ("right", Variable addID "right" In Nothing Nothing Parameter (DefinableType (Just parent) "Real" ) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just parent) "Logical" ) Intrinsic Public Required False)
                                                                     ]) "Real_Eq"[
                                                                         Var addID "left",
                                                                         Var addID "Right"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just parent) "Real" ) Intrinsic Public Required False),
                                                                        ("right", Variable addID "right" In Nothing Nothing Parameter (DefinableType (Just parent) "Real" ) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just parent) "Logical" ) Intrinsic Public Required False)
                                                                     ]) "Real_NotEq"[
                                                                         Var addID "left",
                                                                         Var addID "Right"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just parent) "Real" ) Intrinsic Public Required False),
                                                                        ("right", Variable addID "right" In Nothing Nothing Parameter (DefinableType (Just parent) "Real" ) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just parent) "Logical" ) Intrinsic Public Required False)
                                                                     ]) "Real_Lt"[
                                                                         Var addID "left",
                                                                         Var addID "Right"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just parent) "Real" ) Intrinsic Public Required False),
                                                                        ("right", Variable addID "right" In Nothing Nothing Parameter (DefinableType (Just parent) "Real" ) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just parent) "Logical" ) Intrinsic Public Required False)
                                                                     ]) "Real_LtE"[
                                                                         Var addID "left",
                                                                         Var addID "Right"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just parent) "Real" ) Intrinsic Public Required False),
                                                                        ("right", Variable addID "right" In Nothing Nothing Parameter (DefinableType (Just parent) "Real" ) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just parent) "Logical") Intrinsic Public Required False)
                                                                     ]) "Real_Gt"[
                                                                         Var addID "left",
                                                                         Var addID "Right"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just parent) "Real" ) Intrinsic Public Required False),
                                                                        ("right", Variable addID "right" In Nothing Nothing Parameter (DefinableType (Just parent) "Real" ) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just parent) "Logical" ) Intrinsic Public Required False)
                                                                     ]) "Real_GtE"[
                                                                         Var addID "left",
                                                                         Var addID "Right"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing
                                                        
                                                        ]
                                                    where 
                                                        addID = getNextSymbolTableID program