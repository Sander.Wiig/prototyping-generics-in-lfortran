module Intrinsics.Integer where
import CommonDefinitions
import XASR
import qualified ASR

--pain
integerSignature::ParentSymbolTable->Program->Symbol
integerSignature parent program= Signature parent [DefinableType parent "Integer", DefinableType parent "Logical" ]  [
                                                        --Add | Sub | Mul | Div | Pow
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just parent) "Integer"  ) Intrinsic Public Required False),
                                                                        ("right", Variable addID "right" In Nothing Nothing Parameter (DefinableType (Just parent) "Integer"  ) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just parent) "Integer" ) Intrinsic Public Required False)
                                                                     ]) "Integer_Add"[
                                                                         Var addID "left",
                                                                         Var addID "Right"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just parent) "Integer" ) Intrinsic Public Required False),
                                                                        ("right", Variable addID "right" In Nothing Nothing Parameter (DefinableType (Just parent) "Integer" ) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just parent) "Integer" ) Intrinsic Public Required False)
                                                                     ]) "Integer_Sub"[
                                                                         Var addID "left",
                                                                         Var addID "Right"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just parent) "Integer" ) Intrinsic Public Required False),
                                                                        ("right", Variable addID "right" In Nothing Nothing Parameter (DefinableType (Just parent) "Integer" ) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just parent) "Integer" ) Intrinsic Public Required False)
                                                                     ]) "Integer_Mul"[
                                                                         Var addID "left",
                                                                         Var addID "Right"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just parent) "Integer" ) Intrinsic Public Required False),
                                                                        ("right", Variable addID "right" In Nothing Nothing Parameter (DefinableType (Just parent) "Integer" ) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just parent) "Integer" ) Intrinsic Public Required False)
                                                                     ]) "Integer_Div"[
                                                                         Var addID "left",
                                                                         Var addID "Right"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just parent) "Integer" ) Intrinsic Public Required False),
                                                                        ("right", Variable addID "right" In Nothing Nothing Parameter (DefinableType (Just parent) "Integer" ) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just parent) "Integer" ) Intrinsic Public Required False)
                                                                     ]) "Integer_Pow"[
                                                                         Var addID "left",
                                                                         Var addID "Right"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        --Invert | Not | UAdd | USub
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just parent) "Integer" ) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just parent) "Integer" ) Intrinsic Public Required False)
                                                                     ]) "Integer_UAdd"[
                                                                         Var addID "left"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just parent) "Integer" ) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just parent) "Integer" ) Intrinsic Public Required False)
                                                                     ]) "Integer_USub"[
                                                                         Var addID "left"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        --Eq | NotEq | Lt | LtE | Gt | GtE
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just parent) "Integer" ) Intrinsic Public Required False),
                                                                        ("right", Variable addID "right" In Nothing Nothing Parameter (DefinableType (Just parent) "Integer" ) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just parent) "Logical" ) Intrinsic Public Required False)
                                                                     ]) "Integer_Eq"[
                                                                         Var addID "left",
                                                                         Var addID "Right"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just parent) "Integer" ) Intrinsic Public Required False),
                                                                        ("right", Variable addID "right" In Nothing Nothing Parameter (DefinableType (Just parent) "Integer" ) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just parent) "Logical" ) Intrinsic Public Required False)
                                                                     ]) "Integer_NotEq"[
                                                                         Var addID "left",
                                                                         Var addID "Right"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just parent) "Integer" ) Intrinsic Public Required False),
                                                                        ("right", Variable addID "right" In Nothing Nothing Parameter (DefinableType (Just parent) "Integer" ) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just parent) "Logical" ) Intrinsic Public Required False)
                                                                     ]) "Integer_Lt"[
                                                                         Var addID "left",
                                                                         Var addID "Right"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just parent) "Integer" ) Intrinsic Public Required False),
                                                                        ("right", Variable addID "right" In Nothing Nothing Parameter (DefinableType (Just parent) "Integer" ) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just parent) "Logical" ) Intrinsic Public Required False)
                                                                     ]) "Integer_LtE"[
                                                                         Var addID "left",
                                                                         Var addID "Right"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just parent) "Integer" ) Intrinsic Public Required False),
                                                                        ("right", Variable addID "right" In Nothing Nothing Parameter (DefinableType (Just parent) "Integer" ) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just parent) "Logical" ) Intrinsic Public Required False)
                                                                     ]) "Integer_Gt"[
                                                                         Var addID "left",
                                                                         Var addID "Right"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing,
                                                        Function (SymbolTable (Just parent) addID [
                                                                        ("left", Variable addID "left" In Nothing Nothing Parameter (DefinableType (Just parent) "Integer" ) Intrinsic Public Required False),
                                                                        ("right", Variable addID "right" In Nothing Nothing Parameter (DefinableType (Just parent) "Integer" ) Intrinsic Public Required False),
                                                                        ("return", Variable addID "return" ReturnVar Nothing Nothing Parameter (DefinableType (Just parent) "Logical" ) Intrinsic Public Required False)
                                                                     ]) "Integer_GtE"[
                                                                         Var addID "left",
                                                                         Var addID "Right"
                                                                     ] [] (Var addID "return") Intrinsic Public Interface True Nothing
                                                        ]
                                                    where 
                                                        addID = getNextSymbolTableID program