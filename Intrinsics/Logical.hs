module Intrinsics.Logical where
import CommonDefinitions
import XASR
import qualified ASR

logicalSignature::ParentSymbolTable->Program->Symbol
logicalSignature parent program = Signature parent [DefinableType (Just parent) "Logical" ] "Logical" [
                                                                                                        {-Functions here-}
                                                                                                   ]