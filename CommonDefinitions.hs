{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE ConstrainedClassMethods #-}
module CommonDefinitions where
--ASDL extended types
--Module for all the types shared by both ASR and XASR.

type Identifier = String
type SymbolName = Identifier

type ParentSymbolTable = Int

data StorageType = Default | Save | Parameter | Allocatable deriving (Show, Eq)
data Access = Public | Private deriving (Show, Eq)
data Intent = Local | In | Out | InOut | ReturnVar | Unspecified | AssociateBlock deriving (Show, Eq)
data DefType = Implementation | Interface deriving (Show, Eq)
data Presence = Required | Optional deriving (Show, Eq)

data Abi                   -- External     Abi
    = Source          --   No         Unspecified
    | LFortranModule  --   Yes        LFortran
    | GFortranModule  --   Yes        GFortran
    | BindC           --   Yes        C
    | Interactive     --   Yes        Unspecified
    | Intrinsic       --   Yes        Unspecified
    deriving (Show, Eq)
    
data Boz = Binary | Hex | Octal deriving (Show, Eq)

data Attribute = Attribute Identifier [AttributeArg] deriving (Eq)

type AttributeArg = Identifier

type Arg = Identifier

data Tbind = Bind String String deriving (Eq)

instance Show Attribute where
    show (Attribute id args) = "(Attribute "++show id ++ " "++ show args ++ ")"

instance Show Tbind where
    show (Bind s1 s2) = "(Bind "++s1 ++ " "++s2++")"
