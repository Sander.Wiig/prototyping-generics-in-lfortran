{-# LANGUAGE DeriveAnyClass #-}
module ASR where

--Some definitions are missing, check Common definitions for those not found here

import CommonDefinitions
import Data.Maybe

data SymbolTable = SymbolTable (Maybe Int) Int [(Identifier, Symbol)] deriving (Show, Eq)
data TranslationUnit = TranslationUnit SymbolTable [Node] deriving (Show, Eq)
data Node = Symbol Symbol | Stmt Stmt | Expr Expr deriving (Show, Eq)

data Symbol
    = Program SymbolTable Identifier [Identifier] [Stmt]
    | Module SymbolTable Identifier [Identifier] Bool
    | Subroutine SymbolTable Identifier [Expr] [Stmt]
        Abi Access DefType (Maybe String) Bool Bool
    | Function SymbolTable Identifier [Expr] [Stmt]
        Expr Abi Access DefType Bool (Maybe String)
    | GenericProcedure ParentSymbolTable Identifier [Int] Access
    | CustomOperator ParentSymbolTable Identifier [Int] Access
    | ExternalSymbol ParentSymbolTable Identifier Int String Identifier [Identifier] Identifier Access
    | DerivedType SymbolTable Identifier [Identifier]
        Abi Access (Maybe Int)
    | Variable ParentSymbolTable Identifier Intent (Maybe Expr) (Maybe Expr) StorageType TType Abi Access Presence Bool
    | ClassType SymbolTable Identifier Abi Access
    | ClassProcedure ParentSymbolTable Identifier Identifier SymbolName Abi
    deriving (Show, Eq)

data Stmt
    = Allocate [AllocArg] (Maybe Expr)
    | Assign Int Identifier
    | Assignment Expr Expr (Maybe Stmt)
    | Associate Expr Expr
    | Cycle 
    -- deallocates if allocated otherwise throws a runtime error
    | ExplicitDeallocate [SymbolName]
    -- deallocates if allocated otherwise does nothing
    | ImplicitDeallocate [SymbolName]
    | DoConcurrentLoop DoLoopHead [Stmt]
    | DoLoop DoLoopHead [Stmt]
    | ErrorStop (Maybe Expr)
    | Exit
    | ForAllSingle DoLoopHead Stmt
        -- GoTo points to a GoToTarget with the corresponding target_id within
        -- the same procedure. We currently use `int` IDs to link GoTo with
        -- GoToTarget to avoid issues with serialization.
    | GoTo Int
        -- An empty statement, a target of zero or more GoTo statements
        -- the `id` is only unique within a procedure
    | GoToTarget Int
    | If Expr [Stmt] [Stmt]
    | IfArithmetic Expr Int Int Int
    | Print (Maybe Expr) [Expr]
    | Open Int (Maybe Expr) (Maybe Expr) (Maybe Expr)
    | Close Int (Maybe Expr) (Maybe Expr) (Maybe Expr) (Maybe Expr) (Maybe Expr)
    | Read Int (Maybe Expr) (Maybe Expr) (Maybe Expr) (Maybe Expr) (Maybe Expr) [Expr]
    | Write Int (Maybe Expr) (Maybe Expr) (Maybe Expr) (Maybe Expr) (Maybe Expr) [Expr]
    | Rewind Int (Maybe Expr) (Maybe Expr) (Maybe Expr) 
    | Inquire Int (Maybe Expr) (Maybe Expr) (Maybe Expr) (Maybe Expr)
                (Maybe Expr) (Maybe Expr) (Maybe Expr) (Maybe Expr) 
                (Maybe Expr) (Maybe Expr) (Maybe Expr) (Maybe Expr) 
                (Maybe Expr) (Maybe Expr) (Maybe Expr) (Maybe Expr) 
                (Maybe Expr) (Maybe Expr) (Maybe Expr) (Maybe Expr) 
                (Maybe Expr) (Maybe Expr) (Maybe Expr) (Maybe Expr) 
                (Maybe Expr) (Maybe Expr) (Maybe Expr) (Maybe Expr) 
                (Maybe Expr) (Maybe Expr)--https://www.youtube.com/watch?v=1ZjUiKeXeOc
    | Return
    | Select Expr [CaseStmt] [Stmt]
    | Stop (Maybe Expr)
    | SubroutineCall Int String (Maybe SymbolName) [Expr] (Maybe Expr)
    | Where Expr [Stmt] [Stmt]
    | WhileLoop Expr [Stmt]
    | Nullify [SymbolName]
    | Flush Int Expr (Maybe Expr) (Maybe Expr) (Maybe Expr)  
    deriving (Show, Eq)
-- BinOp (ConstantInteger 1 (Integer 8 [])) Add (ConstantInteger 2 (Integer 8 [])) (Integer 8 []) (Nothing) (Nothing)

data Expr
    = BoolOp Expr Boolop Expr TType (Maybe Expr)
    | BinOp Expr Binop Expr TType (Maybe Expr) (Maybe Expr)
    | StrOp Expr Strop Expr TType (Maybe Expr)
    | UnaryOp Unaryop Expr TType (Maybe Expr)
        -- Such as: (x, y+z), (3.0, 2.0) generally not known at compile time
    | ComplexConstructor Expr Expr TType  (Maybe Expr)
    | Compare Expr Cmpop Expr TType (Maybe Expr) (Maybe Expr)
    | FunctionCall Int String (Maybe SymbolName) [Expr] [Keyword] TType (Maybe Expr) (Maybe Expr)
    | DerivedTypeConstructor Int String [Expr] TType
    | ConstantArray [Expr] TType
    | ImpliedDoLoop [Expr] Expr Expr Expr (Maybe Expr) TType (Maybe Expr)
    | ConstantInteger Int TType
    | ConstantReal Float TType
    | ConstantComplex Float Float TType
    | ConstantLogical Bool TType
    | ConstantString String TType
    | ConstantSet [Expr] TType
    | ConstantDictionary [Expr] [Expr] TType
    | BOZ Int Boz (Maybe TType)
    | Var Int String 
    | ArrayRef Int String [ArrayIndex] TType (Maybe Expr)
    | DerivedRef Expr SymbolName TType (Maybe Expr)
    | ImplicitCast Expr CastKind TType (Maybe Expr)
    | ExplicitCast Expr CastKind TType (Maybe Expr)
    deriving (Show, Eq)

data CastKind
    = RealToInteger
    | IntegerToReal
    | RealToReal
    | IntegerToInteger
    | RealToComplex
    | IntegerToComplex
    | IntegerToLogical
    | ComplexToComplex
    | ComplexToReal
    deriving (Show, Eq)
    
data TType
    = Integer Int [Dimension]
    | Real Int [Dimension]
    | Complex Int [Dimension]
    | Character Int Int (Maybe Expr) [Dimension]
    | Logical Int [Dimension]
    | Derived SymbolName [Dimension]
    | Class SymbolName [Dimension]
    | Pointer TType
    | Dict TType TType
    | Set TType
    deriving (Show, Eq)

data Boolop = And | Or | Xor | NEqv | Eqv deriving (Show, Eq)

data Binop = Add | Sub | Mul | Div | Pow deriving (Show, Eq)

data Unaryop = Invert | Not | UAdd | USub deriving (Show, Eq)

data Strop = Concat | Repeat deriving (Show, Eq)

data Cmpop = Eq | NotEq | Lt | LtE | Gt | GtE deriving (Show, Eq)

type ArrayIndex = (Maybe Expr, Maybe Expr, Maybe Expr)

type DoLoopHead = (Expr, Expr, Expr, Maybe Expr)


type Dimension = (Maybe Expr, Maybe Expr)
type AllocArg = (SymbolName, [Dimension])
type Keyword = (Maybe Identifier, Expr)
data CaseStmt = CaseStmt [Expr] [Stmt] | CaseStmtRange (Maybe Expr) (Maybe Expr) [Stmt] deriving (Show, Eq)


findSymbolTable ::ParentSymbolTable->SymbolTable ->Maybe SymbolTable 
findSymbolTable i (SymbolTable k n maps)
        | i == n = Just (SymbolTable k n maps)
        | all isNothing symTabs = Nothing
        | length table > 1 = error $ "Ambigious Symbol Table with id:" ++ show i
        | otherwise = head table
        where
            symTabs = map (findSymbolTable i . getSymbolTable . snd) maps
            table = filter isJust symTabs

getSymbolTable::Symbol -> SymbolTable
getSymbolTable (Program s _ _ _) = s 
getSymbolTable (Module s _ _ _) =s
getSymbolTable (Subroutine s _ _ _ _ _ _ _ _ _)=s
getSymbolTable (Function s _ _ _ _ _ _ _ _ _) =s
getSymbolTable( ClassType s _ _ _) = s
getSymbolTable _=emptySymbolTable

emptySymbolTable :: SymbolTable 
emptySymbolTable = SymbolTable Nothing 0 []

getAllSymbolsInTable ::SymbolTable ->[(Identifier, Symbol)]
getAllSymbolsInTable (SymbolTable _ _ maps) = maps ++ concatMap (getAllSymbolsInTable . getSymbolTable . snd) maps

getNextSymbolTableID::SymbolTable->Int
getNextSymbolTableID (SymbolTable _ id maps) = foldr1
                                            (\ x y -> if x >= y then x else y)
                                            (id : map (getNextSymbolTableID . getSymbolTable . snd) maps)
                                            + 1
showTypeName ::TType->String
showTypeName (Integer {}) ="Integer"
showTypeName (Real {})="Real" 
showTypeName (Complex {})="Complex"
showTypeName (Character {})= "Character"
showTypeName (Logical {})= "Logical" 
showTypeName (Derived name _)= name
showTypeName (Class name _)= name
showTypeName (Pointer {})="Pointer"
showTypeName (Dict {})="Dict"
showTypeName (Set {})="Set"

