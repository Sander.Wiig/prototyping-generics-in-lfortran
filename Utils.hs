module Utils where
import ASR
import XASR
import CommonDefinitions

--Find signature associated with the TType
lookupSignature :: ASR.TType -> Program->XASR.Symbol
lookupSignature tp= lookupSymbol (show tp)

--Find a symbol in the program based on SymbolID
lookupSymbol :: SymbolName ->Program ->XASR.Symbol
lookupSymbol sym_id program@(XASR.SymbolTable _ symTab_id map) = case lookup sym_id $ XASR.getAllSymbolsInTable program of
                                                    Nothing-> error $ "Can't find symbol with id: " ++ sym_id
                                                    Just s -> s

lookUpSymbolInSignature::XASR.Symbol->Identifier->XASR.Symbol
lookUpSymbolInSignature signature func_name = undefined

