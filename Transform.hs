{-# LANGUAGE LambdaCase #-}
module Transform where
import ASR
import qualified XASR
import CommonDefinitions
import qualified Control.Arrow as Data.Bifunctor
import Data.Maybe
import Data.List
import Utils
import Intrinsics.Integer
import Intrinsics.Real
import Intrinsics.Logical
type Program = XASR.Program

transform::ASR.TranslationUnit ->XASR.TranslationUnit 
transform (TranslationUnit symtab nodes) = XASR.TranslationUnit program (map (`transformNode` program) nodes) (SymbolTable Nothing id [
                                                                                                                                                            ("Integer", integerSignature id program),
                                                                                                                                                            ("Real", realSignature id program),
                                                                                                                                                            ("Logical", logicalSignature) 
                                                                                                                                                        ]) 
    where 
                                    program = transformSymbolTable symtab
                                    id = getNextSymbolTableID program

transformNode :: ASR.Node ->Program -> XASR.Node
transformNode (ASR.Symbol symbol) _= XASR.Symbol (transformSymbol symbol)
transformNode (ASR.Stmt stmt) program= XASR.Stmt (transformStmt stmt program)
transformNode (ASR.Expr expr) program= XASR.Expr (transformExpr expr program)

transformSymbolTable :: ASR.SymbolTable -> XASR.SymbolTable 
transformSymbolTable (ASR.SymbolTable (Just parent_symtab) id mp ) = XASR.SymbolTable (Just parent_symtab) id (map (Data.Bifunctor.second transformSymbol) mp)
transformSymbolTable (ASR.SymbolTable Nothing id mp ) = XASR.SymbolTable Nothing id (map (Data.Bifunctor.second transformSymbol) mp) 

--transformSymbol does not need to be passed a symtab since it is already part of the symbol
transformSymbol :: ASR.Symbol->XASR.Symbol
transformSymbol (ASR.Program symbol_table name dependencies body)= XASR.Program st name dependencies (map (`transformStmt` st) body) where st = transformSymbolTable symbol_table
transformSymbol (ASR.Module symbol_table name dependencies loaded_from_mod)= XASR.Module (transformSymbolTable symbol_table) name dependencies loaded_from_mod
transformSymbol (ASR.Subroutine symbol_table name args body abi access deftype bindc_name pure ismodule)= XASR.Subroutine st name (map (`transformExpr` st) args) (map (`transformStmt` st) body) abi access deftype bindc_name pure ismodule where st = transformSymbolTable symbol_table
transformSymbol (ASR.Function symbol_table name args body return_var abi access deftype pure bindc_name)= XASR.Function st name (map (`transformExpr` st) args) (map (`transformStmt` st) body) (transformExpr return_var st) abi access deftype pure bindc_name where st = transformSymbolTable symbol_table
transformSymbol (ASR.GenericProcedure symbol_table name procs access)= XASR.GenericProcedure symbol_table name procs access
transformSymbol (ASR.CustomOperator symbol_table name procs access) = XASR.CustomOperator symbol_table name procs access
transformSymbol (ASR.ExternalSymbol symboltable name external_id external_name module_name scope_names original_name access)= XASR.ExternalSymbol symboltable name external_id external_name module_name scope_names original_name access
transformSymbol (ASR.DerivedType symbol_table name members abi access Nothing) = XASR.DerivedType (transformSymbolTable symbol_table) name members abi access Nothing
transformSymbol (ASR.DerivedType symbol_table name members abi access symbol) = XASR.DerivedType (transformSymbolTable symbol_table) name members abi access symbol
transformSymbol (ASR.Variable parent_symtab name intent symbolic_val value storage ttype abi access presence value_attr) = XASR.Variable parent_symtab name intent (transformMaybeExpr symbolic_val XASR.emptySymbolTable) (transformMaybeExpr value XASR.emptySymbolTable) storage (transformTType ttype XASR.emptySymbolTable) abi access presence value_attr
transformSymbol (ASR.ClassType symtab name abi access) = undefined --TODO: Need help with this, presumably it would be a derived type of some sort

transformStmt :: ASR.Stmt -> Program->XASR.Stmt
transformStmt (ASR.Allocate args stat) program = XASR.Allocate (map (`transformAllocArg` program) args ) (transformMaybeExpr stat program)
transformStmt (ASR.Assign label variable) program = XASR.Assign label variable 
transformStmt (ASR.Assignment target value (Just overloaded)) program = XASR.Assignment (transformExpr target program) (transformExpr value program) (Just (transformStmt overloaded program))
transformStmt (ASR.Assignment target value Nothing) program = XASR.Assignment (transformExpr target program) (transformExpr value program) Nothing
transformStmt (ASR.Associate target value) program = XASR.Associate (transformExpr target program) (transformExpr value program)
transformStmt ASR.Cycle program = XASR.Cycle 
transformStmt (ASR.ExplicitDeallocate vars) program = XASR.Deallocate vars
transformStmt (ASR.ImplicitDeallocate vars) program = XASR.Deallocate vars
transformStmt (ASR.DoConcurrentLoop head body) program = XASR.DoConcurrentLoop (transformDoLoopHead head program) (map (`transformStmt` program) body)
transformStmt (ASR.DoLoop head body) program = XASR.DoLoop (transformDoLoopHead head program) (map (`transformStmt` program) body)
transformStmt (ASR.ErrorStop (Just expr)) program = XASR.ErrorStop (Just (transformExpr expr program))
transformStmt (ASR.ErrorStop Nothing) program = XASR.ErrorStop Nothing
transformStmt ASR.Exit program= XASR.Exit
transformStmt (ASR.ForAllSingle head assign_stmt) program= XASR.ForAllSingle (transformDoLoopHead head program) (transformStmt assign_stmt program)
transformStmt (ASR.GoTo target_id) program= XASR.GoTo target_id
transformStmt (ASR.GoToTarget id) program= XASR.GoToTarget id
transformStmt (ASR.If test stmts orelse) program= XASR.If (transformExpr test program) (map (`transformStmt` program) stmts) (map (`transformStmt` program) orelse)
transformStmt (ASR.IfArithmetic test lt_label eq_label gt_label) program=XASR.IfArithmetic (transformExpr test program) lt_label eq_label gt_label
transformStmt (ASR.Print fmt values) program= XASR.SubroutineCall (-1) "IO_Print" Nothing  (maybeToJust(map (`transformMaybeExpr` program) (fmt: map Just values))) Nothing
transformStmt (ASR.Open label newunit filename status) program= XASR.SubroutineCall (-1) "IO_Open" Nothing  (maybeToJust(map (`transformMaybeExpr` program) [newunit, filename, status])) Nothing
transformStmt (ASR.Close label unit iostat iomsg err status) program= XASR.SubroutineCall (-1) "IO_Close" Nothing  (maybeToJust(map (`transformMaybeExpr` program) [unit, iostat, iomsg, err, status])) Nothing
transformStmt (ASR.Read label unit fmt iomsg iostat id values) program=XASR.SubroutineCall (-1) "IO_Read" Nothing  (maybeToJust(map (`transformMaybeExpr` program) ([unit, fmt, iomsg, iostat, id]++map Just values))) Nothing
transformStmt (ASR.Write label unit fmt iomsg iostat id values) program= XASR.SubroutineCall (-1) "IO_Write" Nothing  (maybeToJust(map (`transformMaybeExpr` program) ([unit, fmt, iomsg, iostat, id]++map Just values))) Nothing
transformStmt (ASR.Inquire label unit file iostat err exist opened number named 
                            name access sequential direct form formatted unformatted 
                            recl nextrec blank position action read write readwrite 
                            delim pad flen blocksize convert carriagecontrol iolength) program= XASR.SubroutineCall (-1) "IO_Inquire" Nothing  (maybeToJust(map (`transformMaybeExpr` program) [unit, file, iostat, err, exist, opened, number, named, 
                                                                                                                                            name, access, sequential, direct, form, formatted, unformatted, 
                                                                                                                                            recl, nextrec, blank, position, action, read, write, readwrite, 
                                                                                                                                            delim, pad, flen, blocksize, convert, carriagecontrol, iolength])) Nothing  --https://www.youtube.com/watch?v=3tmd-ClpJxA
transformStmt (ASR.Rewind label unit iostat err) program = XASR.SubroutineCall (-1) "IO_Rewind" Nothing (maybeToJust$(map (`transformMaybeExpr` program) [unit, iostat, err])) Nothing

transformStmt ASR.Return symtab= XASR.Return
transformStmt (ASR.Select test body def) symtab= XASR.Select (transformExpr test symtab) (map (`transformCaseStmt` symtab) body) (map (`transformStmt` symtab) def )
transformStmt (ASR.Stop code) symtab= XASR.Stop (transformMaybeExpr code symtab)
transformStmt (ASR.SubroutineCall id name original_name args dt) symtab= XASR.SubroutineCall id name original_name (map (`transformExpr` symtab) args) (transformMaybeExpr dt symtab)
transformStmt (ASR.Where test body orelse) symtab= XASR.Where (transformExpr test symtab) (map (`transformStmt` symtab) body) (map (`transformStmt` symtab) orelse)
transformStmt (ASR.WhileLoop test body) symtab= XASR.WhileLoop (transformExpr test symtab) (map (`transformStmt` symtab) body)

transformStmt (ASR.Nullify syms) program = XASR.Nullify syms
transformStmt (ASR.Flush label unit err iomsg iostat) program = XASR.SubroutineCall (-1) "IO_Flush" Nothing (maybeToJust (map ( `transformMaybeExpr` program) [Just unit, err, iomsg, iostat])) Nothing

--handles maybe exprs
transformMaybeExpr :: Maybe ASR.Expr ->Program ->Maybe XASR.Expr 
transformMaybeExpr Nothing  _= Nothing 
transformMaybeExpr (Just expr) program = Just (transformExpr expr program) 

--Things we need!
--Need ASR reps for function versions of these expressions
transformExpr :: ASR.Expr ->Program -> XASR.Expr--TODO: Make all function calls reference signature symtab
transformExpr (ASR.BoolOp left op right tp value) program= XASR.FunctionCall (-1) (showTypeName tp++"_"++show op) Nothing  [transformExpr left program, transformExpr right program] [] (transformTType tp program) Nothing Nothing
transformExpr (ASR.BinOp left op right tp value overloaded) program = XASR.FunctionCall (-1) (showTypeName tp++"_"++show op) Nothing  [transformExpr left program, transformExpr right program] [] (transformTType tp program) Nothing Nothing
transformExpr(ASR.StrOp left op right tp value) program= XASR.FunctionCall (-1) (showTypeName tp++"_"++show op) Nothing  [transformExpr left program, transformExpr right program] [] (transformTType tp program) Nothing Nothing
transformExpr(ASR.UnaryOp op operand tp value) program = XASR.FunctionCall (-1) (showTypeName tp++"_"++show op) Nothing  [transformExpr operand program] [] (transformTType tp program) Nothing Nothing

transformExpr(ASR.ComplexConstructor re im tp value) program = undefined
transformExpr(ASR.Compare left op right tp value overloaded) program= XASR.FunctionCall (-1) (showTypeName tp++"_"++show op) Nothing  [transformExpr left program, transformExpr right program] [] (transformTType tp program) Nothing Nothing

transformExpr(ASR.FunctionCall id name original_name args keywords tp value dt) program = XASR.FunctionCall id name original_name (map (`transformExpr` program) args) (map (`transformKeyword` program) keywords) (transformTType tp program) (transformMaybeExpr value program) (transformMaybeExpr dt program)
transformExpr(ASR.DerivedTypeConstructor id dt_symb args tp) program= XASR.DerivedTypeConstructor id dt_symb (map (`transformExpr` program) args) (transformTType tp program) 
transformExpr(ASR.ConstantArray args tp) program= XASR.ConstantArray (map transformExpr args) (XASR.Array [] (transformTType tp))--TODO:This
transformExpr(ASR.ImpliedDoLoop values var start end increment tp value) program= XASR.ImpliedDoLoop (map (`transformExpr` program) values) (transformExpr var program) (transformExpr start program) (transformExpr end program) (transformMaybeExpr increment program) (transformTType tp program) (transformMaybeExpr value program)
transformExpr(ASR.ConstantInteger n tp) program= XASR.Constant (transformTType tp program) (show n)
transformExpr(ASR.ConstantReal r tp) program= XASR.Constant (transformTType tp program) (show r)
transformExpr(ASR.ConstantComplex re im tp) program= XASR.Constant (transformTType tp program) (show re++":"++show im)
transformExpr(ASR.ConstantLogical value tp) program= XASR.Constant (transformTType tp program) (show value)
transformExpr(ASR.ConstantString s tp) program= XASR.Constant (transformTType tp program) s
transformExpr(ASR.BOZ v boz (Just tp)) program= XASR.Constant (transformTType tp program) (show v++show boz)
transformExpr(ASR.BOZ v boz Nothing ) program= error "No type given"
transformExpr(ASR.Var id v) program= XASR.Var id v
transformExpr(ASR.ArrayRef i v args tp value) program= XASR.ArrayRef i v (map (`transformArrayIndex` program) args) (transformTType tp program) (transformMaybeExpr value program)
transformExpr(ASR.DerivedRef v m tp value) program= XASR.DerivedRef v m (transformTType tp) (transformExpr value)
transformExpr(ASR.ImplicitCast arg kind tp value) program= undefined
transformExpr(ASR.ExplicitCast arg kind tp value) program= undefined

transformCaseStmt ::ASR.CaseStmt ->Program->XASR.CaseStmt
transformCaseStmt (ASR.CaseStmt xs ss) program= XASR.CaseStmt (map (`transformExpr` program) xs) (map (`transformStmt` program) ss)
transformCaseStmt (ASR.CaseStmtRange e e1 ss) program= XASR.CaseStmtRange (transformMaybeExpr e program) (transformMaybeExpr e1 program) (map (`transformStmt` program) ss)

transformAllocArg :: ASR.AllocArg ->Program->XASR.AllocArg
transformAllocArg (symbol, dims)program = (symbol, map (`transformDimension` program) dims)

transformArrayIndex::ASR.ArrayIndex->Program->XASR.ArrayIndex
transformArrayIndex (left, right,step ) program = (transformMaybeExpr left program, transformMaybeExpr right program, transformMaybeExpr step program)

transformDimension :: ASR.Dimension ->Program->XASR.Dimension
transformDimension (exprs, exprs') program=((transformMaybeExpr exprs program), (transformMaybeExpr exprs' program))

transformDoLoopHead :: ASR.DoLoopHead->Program->XASR.DoLoopHead 
transformDoLoopHead (v, start, end, Just increment) program= (transformExpr v program, transformExpr start program, transformExpr end program, Just (transformExpr increment program))
transformDoLoopHead (v, start, end, Nothing) program= (transformExpr v program, transformExpr start program, transformExpr end program, Nothing)

maybeToJust::[Maybe a]->[a]
maybeToJust [] = []
maybeToJust ((Just a):xs) = [a]++maybeToJust xs 
maybeToJust (Nothing:xs)=maybeToJust xs

{-
    In most cases will return Definable Type with the symbolID of the accompanying signature
-}
transformTType :: ASR.TType -> Program ->XASR.TType 
transformTType (ASR.Integer kind []) program = XASR.DefinableType (Just kind) "Integer"
transformTType (ASR.Integer kind dims) program = XASR.Array(XASR.DefinableType (Just kind) "Integer" ) (map (`transformDimension` program) dims)

transformTType (ASR.Real kind []) program = XASR.DefinableType (Just kind) "Real" (map (`transformDimension` program) dims)
transformTType (ASR.Real kind dims) program = XASR.Array(XASR.DefinableType (Just kind) "Real") (map (`transformDimension` program) dims)

transformTType (ASR.Complex kind []) program = XASR.DefinableType (Just kind) "Complex" 
transformTType (ASR.Complex kind dims) program = XASR.Array(XASR.DefinableType (Just kind) "Complex") (map (`transformDimension` program) dims)

transformTType (ASR.Logical kind []) program = XASR.DefinableType (Just kind) "Logical" (map (`transformDimension` program) dims)
transformTType (ASR.Logical kind dims) program = XASR.Array(XASR.DefinableType (Just kind) "Logical") (map (`transformDimension` program) dims)

transformTType (ASR.Derived sym []) program = XASR.DefinableType Nothing sym
transformTType (ASR.Derived sym dims) program = XASR.Array(XASR.DefinableType Nothing sym) (map (`transformDimension` program) dims)

transformTType (ASR.Character kind len len_expr dims) program =  undefined--XASR.DefinableType (Just kind) "Char" (map (`transformDimension` program) dims)
transformTType (ASR.Class class_type dims) program = undefined

transformTType (ASR.Pointer tp) program = XASR.Pointer (transformTType tp program)

transformKeyword :: ASR.Keyword ->Program ->XASR.Keyword 
transformKeyword (id, expr) program= (id, transformExpr expr program)
